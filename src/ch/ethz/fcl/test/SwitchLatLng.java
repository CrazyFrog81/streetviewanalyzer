/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class SwitchLatLng {
	public static void main(String[] args) {
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(new File("sdf.xml")));

			BufferedWriter bw = new BufferedWriter(new FileWriter(new File("xxx3")));
			
			String line = br.readLine();
			while(line != null){
				System.out.println(line);
				
				String[] ss = line.split(",");
				bw.write(ss[1]+","+ss[0]+",");
				
				line = br.readLine();
			}

			bw.flush();
			bw.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
