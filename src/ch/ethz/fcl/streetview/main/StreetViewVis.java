/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import ch.ethz.fcl.streetview.config.SVGeneralConfig;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.ethz.fcl.streetview.model.SVModel;
import ch.ethz.fcl.streetview.ui.SVRenderer;
import ch.ethz.fcl.streetview.ui.SVUI;
import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.platform.Platform;
import ch.fhnw.ether.platform.IImageSupport.FileFormat;
import ch.fhnw.ether.render.forward.ForwardRenderer;
import ch.fhnw.ether.scene.DefaultScene;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.view.DefaultView;
import ch.fhnw.ether.view.IView;
import ch.fhnw.ether.view.OffscreenView;
import ch.fhnw.ether.view.IView.Config;
import ch.fhnw.ether.view.IView.ViewType;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Vec3;

public class StreetViewVis {
	public static void main(String[] args) throws InterruptedException {
		new StreetViewVis();
	}

	public StreetViewVis() throws InterruptedException {
		SVModel model = new SVModel();
		SVUI ui = new SVUI(model);

		// Init platform
		Platform.get().init();

		// save screen shot
		if (SVGeneralConfig.GENERATE_SAMPLE_LOCATIONS)
			screen_save(ui, model);

		// Create controller
		IController controller = new DefaultController(
				new SVRenderer(model, ui));

		int start_x = 500, start_y = 0;

		controller.run(time -> {
			IScene scene = new DefaultScene(controller);
			controller.setScene(scene);
			
			// Create view 0
//			IView view0 = new DefaultView(controller, start_x, start_y, 1200,
//					200, new Config(ViewType.RENDER_VIEW, 0, RGBA.BLACK),
//					"A Visual Comparison Approach for Exploring Human-Scale Factors in Cities");
//			ICamera camera0 = getCameraLookingDown(new Vec3(-10, -10, 1));
//			scene.add3DObject(camera0);
//			controller.setCamera(view0, camera0);

			// Create view 1
			IView view1 = new DefaultView(controller, start_x, start_y + 200,
					1200, 800, new Config(ViewType.INTERACTIVE_VIEW, 0, RGBA.BLACK),
					"Left View");

			ICamera camera1 = getCameraLookingDown(
					model.getLeftCity().getCityConfig().getCenter());

			scene.add3DObject(camera1);
			controller.setCamera(view1, camera1);

			// Create view 2
//			IView view2 = new DefaultView(controller, start_x + 600,
//					start_y + 200, 600, 400,
//					new Config(ViewType.RENDER_VIEW, 0, RGBA.BLACK),
//					"Right View");
//			ICamera camera2 = getCameraLookingDown(
//					model.getRightCity().getCityConfig().getCenter());
//			scene.add3DObject(camera2);
//			controller.setCamera(view2, camera2);

			// Create view 3
//			IView view3 = new DefaultView(controller, start_x, start_y + 600,
//					1200, 700, new Config(ViewType.RENDER_VIEW, 0, RGBA.BLACK),
//					"Bottom View");
//			ICamera camera3 = getCameraLookingDown(new Vec3(-10, -10, 1));
//			scene.add3DObject(camera3);
//			controller.setCamera(view3, camera3);

			try {
				ui.enable(controller);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		Platform.get().run();
	}

	private Camera getCameraLookingDown(Vec3 cam_pos) {
		return new Camera(cam_pos, new Vec3(cam_pos.x, cam_pos.y, 0));
	}

	void screen_save(SVUI ui, SVModel model) throws InterruptedException {
		IController offscreen_controller = new DefaultController(
				new ForwardRenderer(false));

		CountDownLatch inited = new CountDownLatch(1);
		offscreen_controller.run(time -> {
			OffscreenView view = new OffscreenView(offscreen_controller, 1500,
					1000, new Config(ViewType.INTERACTIVE_VIEW, 0, RGBA.BLACK));
			IScene scene = new DefaultScene(offscreen_controller);

			offscreen_controller.setScene(scene);
			// Create and add camera
			ICamera camera = new Camera();

			try {
				ui.enable(offscreen_controller);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			for (SVCityModel city_model : model.getCityModels()) {
				camera = getCameraLookingDown(
						city_model.getCityConfig().getCenter());

				offscreen_controller.setCamera(view, camera);
				try {
					offscreen_controller.getRenderManager().update();

					File out = new File(
							city_model.getCityConfig().getPreviewImgLink());
					Platform.get().getImageSupport().write(view.getImage(),
							new FileOutputStream(out), FileFormat.PNG);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			inited.countDown();
		});
		inited.await();
	}
}
