/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui.render;

import java.nio.ByteBuffer;
import java.util.List;

import org.lwjgl.nanovg.NVGPaint;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryUtil;

import ch.ethz.fcl.config.UIFont;
import ch.ethz.fcl.render.NVGData;
import ch.ethz.fcl.render.RendererWithNVG;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.ethz.fcl.streetview.model.gsv_result.SVSample;
import ch.ethz.fcl.streetview.ui.SVRenderer;
import ch.ethz.fcl.streetview.ui.ViewSide;
import ch.ethz.fcl.util.ColorUtil;
import ch.ethz.fcl.util.IOUtil;
import ch.ethz.fcl.util.StringUtil;
import ch.fhnw.ether.view.IView;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;

public class SVMapRenderer {
	SVRenderer render;
	private NVGPaint paint = NVGPaint.create();

	private float directionWidth = 30.0f; // width of north icon in pixels
	public static final float SCALE_LENGTH = 120.0f;

	private int dir_icon = -1;
	private ByteBuffer dir_icon_buffer = null;

	public SVMapRenderer(SVRenderer render) {
		this.render = render;
		dir_icon_buffer = IOUtil.ioResourceToByteBuffer(
				"src/resources/icons/north2.png", 32 * 1024);
	}

	public void render(IView view, SVCityModel city_model, ViewSide side,
			long ctx, Vec2 frame_size) {
		int width = (int) frame_size.x, height = (int) frame_size.y;

		if (dir_icon == -1)
			dir_icon = NanoVG.nvgCreateImageMem(ctx, 0, dir_icon_buffer);

		NanoVG.nvgBeginFrame(ctx, width, height, 1);

		// render a white frame
		NanoVG.nvgBeginPath(ctx);
		NanoVG.nvgMoveTo(ctx, 0, 0);
		NanoVG.nvgLineTo(ctx, width, 0);
		NanoVG.nvgLineTo(ctx, width, height);
		NanoVG.nvgLineTo(ctx, 0, height);
		NanoVG.nvgClosePath(ctx);
		NanoVG.nvgStrokeWidth(ctx, 3);
		NanoVG.nvgStrokeColor(ctx, RendererWithNVG.WHITE);
		NanoVG.nvgStroke(ctx);

		renderCityInfo(city_model, side, ctx, (int) frame_size.x,
				(int) frame_size.y);
		renderPickedSample(city_model, side, ctx, (int) frame_size.x,
				(int) frame_size.y);

		renderMapInfo(view, city_model, ctx, width, height);

		NanoVG.nvgEndFrame(ctx);
	}

	private void renderCityInfo(SVCityModel city_model, ViewSide side, long ctx,
			int frame_w, int frame_h) {
		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_MIDDLE);

		NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(side.getColor()));

		int start_x = 20, start_y = 30;
		NanoVG.nvgText(ctx, start_x, start_y,
				"City: " + city_model.getCityConfig().getCity(),
				MemoryUtil.NULL);
		NanoVG.nvgText(ctx, start_x, start_y + UIFont.MEDIUM,
				"Area: " + city_model.getCityConfig().getSize(),
				MemoryUtil.NULL);
		NanoVG.nvgText(ctx, start_x, start_y + 2 * UIFont.MEDIUM,
				"Sample Size: " + StringUtil
						.formatNum(city_model.getCityConfig().getSampleSize()),
				MemoryUtil.NULL);
	}

	private void renderPickedSample(SVCityModel city_model, ViewSide side,
			long ctx, int frame_w, int frame_h) {
		List<SVSample> samples = city_model.getAnalyzer().getPickSamples();
		int count = 1;

		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_MIDDLE);
		for (SVSample sample : samples) {
			Vec3 sample_pos = sample.sample_screen_pos;
			Vec3 frame_pos = sample.frame_screen_pos;
			if (sample_pos == null || frame_pos == null)
				return;

			// render a circle on the picked sample position
			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgCircle(ctx, sample_pos.x, frame_h - sample_pos.y, 15);
			NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(side.getColor()));
			NanoVG.nvgFill(ctx);

			// render sample number
			NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nvgText(ctx, sample_pos.x, frame_h - sample_pos.y,
					count + "", MemoryUtil.NULL);

			count++;
		}
	}

	private void renderMapInfo(IView view, SVCityModel city_model, long ctx,
			int frame_w, int frame_h) {

		float world_dist = city_model.getScaleWorldDist();

		float x = frame_w - SCALE_LENGTH - 30, y = frame_h - 50;

		// draw lines
		NanoVG.nvgBeginPath(ctx);
		NanoVG.nnvgMoveTo(ctx, x, y - 10);
		NanoVG.nvgLineTo(ctx, x, y);
		NanoVG.nvgLineTo(ctx, x + SCALE_LENGTH, y);
		NanoVG.nvgLineTo(ctx, x + SCALE_LENGTH, y - 10);
		NanoVG.nvgStrokeWidth(ctx, 3);
		NanoVG.nvgStrokeColor(ctx, RendererWithNVG.WHITE);
		NanoVG.nvgStroke(ctx);

		// display world dist
		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_BOTTOM);
		NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
		NanoVG.nvgText(ctx, x + SCALE_LENGTH / 2, y, (int) world_dist + " m",
				MemoryUtil.NULL);

		// display N icon
		NanoVG.nvgImagePattern(ctx, x - directionWidth - 10,
				y - directionWidth - 5, directionWidth, directionWidth * 2, 0,
				dir_icon, 1, paint);
		NanoVG.nvgBeginPath(ctx);
		NanoVG.nvgRect(ctx, x - directionWidth - 10, y - directionWidth - 5,
				directionWidth, directionWidth * 2);
		NanoVG.nvgFillPaint(ctx, paint);
		NanoVG.nvgFill(ctx);
	}
}
