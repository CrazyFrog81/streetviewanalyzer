/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui.render;

import java.nio.ByteBuffer;

import org.lwjgl.nanovg.NVGPaint;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryUtil;

import ch.ethz.fcl.config.UIFont;
import ch.ethz.fcl.render.NVGData;
import ch.ethz.fcl.render.RendererWithNVG;
import ch.ethz.fcl.streetview.config.SVCityConfig;
import ch.ethz.fcl.streetview.model.SVModel;
import ch.ethz.fcl.streetview.ui.SVRenderer;
import ch.ethz.fcl.util.IOUtil;
import ch.fhnw.util.math.Vec2;

public class SVPreviewRenderer {
	SVRenderer render;
	private NVGPaint paint = NVGPaint.create();

	private int[] imgs = null;
	private ByteBuffer[] img_buffers = null;

	public SVPreviewRenderer(SVRenderer render) {
		this.render = render;

		img_buffers = new ByteBuffer[SVCityConfig.values().length];
		int count = 0;
		for (SVCityConfig city_config : SVCityConfig.values()) {
			img_buffers[count++] = IOUtil.ioResourceToByteBuffer(
					city_config.getPreviewImgLink(), 32 * 1024);
		}
	}

	public void render(SVModel model, long ctx, Vec2 frame_size) {
		if (imgs == null) {
			imgs = new int[img_buffers.length];

			int count = 0;
			for (ByteBuffer bb : img_buffers)
				imgs[count++] = NanoVG.nvgCreateImageMem(ctx, 0, bb);
		}

		int frame_w = (int) frame_size.x, frame_h = (int) frame_size.y;
		NanoVG.nvgBeginFrame(ctx, frame_w, frame_h, 1);

		int x = 0, y = 0;
		int img_height = frame_h, img_width = frame_h * 3 / 2;
		int count = 0;
		NanoVG.nnvgFontSize(ctx, UIFont.LARGE);
		NanoVG.nvgFontFace(ctx, NVGData.Font.ADELLE_PE_ITALIC.getID());
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_MIDDLE);
		NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);

		for (SVCityConfig config : SVCityConfig.values()) {
			// render city image
			NanoVG.nvgImagePattern(ctx, x, y, img_width, img_height, 0,
					imgs[count], 1, paint);
			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgRect(ctx, x, y, img_width, img_height);
			NanoVG.nvgFillPaint(ctx, paint);
			NanoVG.nvgFill(ctx);

			// render city name
			NanoVG.nvgText(ctx, x + 20, 30, config.getCity(), MemoryUtil.NULL);

			// render a white frame
			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgMoveTo(ctx, x + img_width, y);
			NanoVG.nvgLineTo(ctx, x + img_width, y + img_height - 8);
			NanoVG.nvgLineTo(ctx, x, y + img_height - 8);
			NanoVG.nnvgStrokeWidth(ctx, 3);
			NanoVG.nvgStrokeColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nvgStroke(ctx);

			x += img_width;
			count++;
		}

		NanoVG.nvgEndFrame(ctx);
	}
}
