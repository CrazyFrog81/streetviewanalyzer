/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui.render;

import java.util.List;

import org.lwjgl.nanovg.NVGColor;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryUtil;

import ch.ethz.fcl.algorithm.spline.SankeyLine;
import ch.ethz.fcl.config.UIFont;
import ch.ethz.fcl.render.NVGData;
import ch.ethz.fcl.render.RendererWithNVG;
import ch.ethz.fcl.streetview.config.SVCoordinate;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.ethz.fcl.streetview.model.SVModel;
import ch.ethz.fcl.streetview.model.gsv_result.SVSample;
import ch.ethz.fcl.streetview.ui.SVRenderer;
import ch.ethz.fcl.streetview.ui.ViewSide;
import ch.ethz.fcl.util.ColorUtil;
import ch.ethz.fcl.util.MathUtil;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec4;

public class SVAnalyerRendererScatterPlot {
	SVRenderer render;

	Vec4 multiple_plot_area, bar_chart_area, sankey_area;
	float single_plot_width;
	int min_value, max_value;

	public SVAnalyerRendererScatterPlot(SVRenderer render) {
		this.render = render;
	}

	public void render(SVModel sv_model, long ctx, Vec2 frame_size) {
		int width = (int) frame_size.x, height = (int) frame_size.y;
		int x = 0, y = 0;

		NanoVG.nvgBeginFrame(ctx, width, height, 1);

		// render a white frame
		NanoVG.nvgBeginPath(ctx);
		NanoVG.nvgMoveTo(ctx, 0, 0);
		NanoVG.nvgLineTo(ctx, width, 0);
		NanoVG.nvgLineTo(ctx, width, height);
		NanoVG.nvgLineTo(ctx, 0, height);
		NanoVG.nvgClosePath(ctx);
		NanoVG.nvgStrokeWidth(ctx, 3);
		NanoVG.nvgStrokeColor(ctx, RendererWithNVG.WHITE);
		NanoVG.nvgStroke(ctx);

		renderScatterPlot(sv_model, ctx, x, y, width, height);
		renderBarChart(sv_model, ctx);
		renderSankeyDiagram(sv_model, ctx, width, height);

		try {
			renderPickedSample(sv_model.getLeftCity(), ViewSide.Left, ctx,
					(int) frame_size.x, (int) frame_size.y);
			renderPickedSample(sv_model.getRightCity(), ViewSide.Right, ctx,
					(int) frame_size.x, (int) frame_size.y);
		} catch (Exception e) {
			e.printStackTrace();
		}
		NanoVG.nvgEndFrame(ctx);
	}

	private void renderPickedSample(SVCityModel city_model, ViewSide side,
			long ctx, int frame_w, int frame_h) throws Exception {
		List<SVSample> samples = city_model.getAnalyzer().getPickSamples();
		int sample_count = 1;

		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.SMALL);
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_MIDDLE);

		float render_x, render_y;

		for (SVSample sample : samples) {
			for (int coord_index = 0; coord_index < SVCoordinate
					.values().length; coord_index++) {
				render_x = multiple_plot_area.x
						+ single_plot_width * coord_index;
				render_y = multiple_plot_area.y
						+ single_plot_width * coord_index;

				float coord_value = sample
						.getResult(SVCoordinate.values()[coord_index].getId());
				coord_value = Math.min(coord_value, max_value);
				float scatter_y = MathUtil.map(coord_value, min_value,
						max_value, render_y + single_plot_width, render_y);

				for (int next_coord_index = coord_index
						+ 1; next_coord_index < SVCoordinate
								.values().length; next_coord_index++) {
					render_x += single_plot_width;

					float next_coord_value = sample.getResult(
							SVCoordinate.values()[next_coord_index].getId());

					next_coord_value = Math.min(next_coord_value, max_value);

					float scatter_x = MathUtil.map(next_coord_value, min_value,
							max_value, render_x, render_x + single_plot_width);

					NanoVG.nvgBeginPath(ctx);
					NanoVG.nvgCircle(ctx, scatter_x, scatter_y, 15);
					NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(side.getColor()));
					NanoVG.nvgFill(ctx);

					NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
					NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
					NanoVG.nvgTextAlign(ctx,
							NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_MIDDLE);
					NanoVG.nvgText(ctx, scatter_x, scatter_y, sample_count + "",
							MemoryUtil.NULL);
				}
			}

			sample_count++;
		}
	}

	private void renderScatterPlot(SVModel sv_model, long ctx, int x, int y,
			int bg_width, int bg_height) {
		float margin = 15;
		float start_x = x + margin, start_y = y + margin,
				height = bg_height - margin * 2;
		float width = height;

		single_plot_width = width / (SVCoordinate.values().length);
		multiple_plot_area = new Vec4(start_x - single_plot_width / 2, start_y,
				width, height);

		int[][] all_distri = sv_model.getLeftCity().getAnalyzer()
				.getFeatureDistribution();
		max_value = all_distri[0].length;
		min_value = 0;

		renderScatterPlotAxes(ctx);
		try {
			renderCityCorrelation(sv_model.getLeftCity(), ViewSide.Left, ctx);
			renderCityCorrelation(sv_model.getRightCity(), ViewSide.Right, ctx);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void renderCityCorrelation(SVCityModel city_model, ViewSide side,
			long ctx) throws Exception {
		float render_x, render_y;
		List<SVSample> results = city_model.getAnalyzer().getSamples();
		int[] pcp_indices = city_model.getAnalyzer().getPCPRandomIndices();
		NVGColor color = ColorUtil.nvgRGBA(side.getColor(), 0.5f);

		for (int i = 0; i < pcp_indices.length; i++) {
			SVSample sample = results.get(i);

			for (int coord_index = 0; coord_index < SVCoordinate
					.values().length; coord_index++) {
				render_x = multiple_plot_area.x
						+ single_plot_width * coord_index;
				render_y = multiple_plot_area.y
						+ single_plot_width * coord_index;

				float coord_value = sample
						.getResult(SVCoordinate.values()[coord_index].getId());
				coord_value = Math.min(coord_value, max_value);
				float scatter_y = MathUtil.map(coord_value, min_value,
						max_value, render_y + single_plot_width, render_y);

				for (int next_coord_index = coord_index
						+ 1; next_coord_index < SVCoordinate
								.values().length; next_coord_index++) {
					render_x += single_plot_width;

					float next_coord_value = sample.getResult(
							SVCoordinate.values()[next_coord_index].getId());

					next_coord_value = Math.min(next_coord_value, max_value);

					float scatter_x = MathUtil.map(next_coord_value, min_value,
							max_value, render_x, render_x + single_plot_width);

					NanoVG.nvgBeginPath(ctx);
					NanoVG.nvgCircle(ctx, scatter_x, scatter_y, 1);
					NanoVG.nvgFillColor(ctx, color);
					NanoVG.nvgFill(ctx);
				}
			}
		}
	}

	private void renderScatterPlotAxes(long ctx) {
		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
		NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_MIDDLE);
		float render_y, render_x;
		int comp_w = 100, comp_h = 50;

		for (int index = 0; index < SVCoordinate.values().length; index++) {
			render_x = multiple_plot_area.x + single_plot_width * index;
			render_y = multiple_plot_area.y + single_plot_width * index;

			for (int j = index; j < SVCoordinate.values().length; j++) {
				// render text labeling
				if (j == index) {
					SVCoordinate coord = SVCoordinate.values()[index];

					float text_x = render_x + single_plot_width / 2;
					if (index == 0) {
						text_x = render_x + single_plot_width - comp_w / 2 - 10;
					}

					NanoVG.nvgBeginPath(ctx);
					NanoVG.nvgRect(ctx, text_x - comp_w / 2,
							render_y + single_plot_width / 2 - comp_h / 2,
							comp_w, comp_h);
					NanoVG.nvgFillColor(ctx,
							ColorUtil.nvgRGB(coord.getColor()));
					NanoVG.nvgFill(ctx);

					NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
					NanoVG.nvgTextAlign(ctx,
							NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_MIDDLE);
					NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
					NanoVG.nvgText(ctx, text_x,
							render_y + single_plot_width / 2, coord.getId(),
							MemoryUtil.NULL);
				} else { // render box
					NanoVG.nvgBeginPath(ctx);
					NanoVG.nvgMoveTo(ctx, render_x, render_y);
					NanoVG.nvgLineTo(ctx, render_x + single_plot_width,
							render_y);
					NanoVG.nvgLineTo(ctx, render_x + single_plot_width,
							render_y + single_plot_width);
					NanoVG.nvgLineTo(ctx, render_x,
							render_y + single_plot_width);
					NanoVG.nvgClosePath(ctx);
					NanoVG.nvgStrokeWidth(ctx, 1);
					NanoVG.nvgStrokeColor(ctx, RendererWithNVG.WHITE);
					NanoVG.nvgStroke(ctx);
				}

				// render axis
				if (j == index + 1) {
					renderScatterAxis(ctx, render_x, render_y);
				}

				render_x += single_plot_width;
			}
		}
	}

	private void renderScatterAxis(long ctx, float x, float y) {
		float label_width = 10;
		int size = 5;
		float gap = single_plot_width / size;
		float render_x = x, render_y = y + gap;

		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.SMALL);
		NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);

		for (int i = 1; i < size; i++) {
			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgRect(ctx, render_x, render_y, label_width, 1);
			NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nvgFill(ctx);

			// render max value
			if (i == 1) {
				NanoVG.nvgTextAlign(ctx,
						NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_TOP);
				NanoVG.nvgText(ctx, render_x, render_y - gap, max_value + "%",
						MemoryUtil.NULL);
			}

			render_y += gap;
		}

		render_x += gap;
		for (int i = 1; i < size; i++) {
			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgRect(ctx, render_x, render_y - label_width, 1,
					label_width);
			NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nnvgFill(ctx);

			// render min value
			if (i == 1) {
				NanoVG.nvgTextAlign(ctx,
						NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_BOTTOM);
				NanoVG.nvgText(ctx, render_x - gap, render_y, min_value + "",
						MemoryUtil.NULL);
			}

			// render max value
			if (i == size - 1) {
				NanoVG.nvgTextAlign(ctx,
						NanoVG.NVG_ALIGN_RIGHT | NanoVG.NVG_ALIGN_BOTTOM);
				NanoVG.nvgText(ctx, render_x + gap, render_y, max_value + "%",
						MemoryUtil.NULL);
			}

			render_x += gap;
		}
	}

	private void renderBarChart(SVModel sv_model, long ctx) {
		float start_x = multiple_plot_area.x + multiple_plot_area.z;
		float start_y = multiple_plot_area.y;
		float width = multiple_plot_area.z / 2;
		float height = multiple_plot_area.w;

		bar_chart_area = new Vec4(start_x, start_y, width, height);

		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.SMALL);
		NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);

		renderDistribution(sv_model.getLeftCity(), ViewSide.Left, ctx);
		renderDistribution(sv_model.getRightCity(), ViewSide.Right, ctx);

		renderBarChartAxes(ctx);
	}

	private void renderDistribution(SVCityModel city_model, ViewSide side,
			long ctx) {
		float y_gap = bar_chart_area.w / SVCoordinate.values().length;
		float render_x = bar_chart_area.x,
				render_y = bar_chart_area.y + y_gap / 2;

		int max_distri = city_model.getAnalyzer().getMaxDistribution();
		float max_height = y_gap / 2 - 10;
		float unit_height = max_height / max_distri;
		float x_gap = (bar_chart_area.z) / (max_value - min_value);

		for (int index = 0; index < SVCoordinate.values().length; index++) {
			int[] distri = city_model.getAnalyzer()
					.getFeatureDistribution()[index];

			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgMoveTo(ctx, render_x, render_y);
			for (int i = 0; i < distri.length; i++) {
				float y = render_y - unit_height * distri[i];
				if (side == ViewSide.Right)
					y = render_y + unit_height * distri[i];

				NanoVG.nvgLineTo(ctx, render_x, y);

				render_x += x_gap;

				NanoVG.nvgLineTo(ctx, render_x, y);
			}

			NanoVG.nvgLineTo(ctx, render_x, render_y);
			NanoVG.nvgClosePath(ctx);
			// NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(side.getColor()));
			NanoVG.nvgFillColor(ctx,
					ColorUtil.nvgRGB(SVCoordinate.values()[index].getColor()));
			NanoVG.nvgFill(ctx);

			render_x = bar_chart_area.x;
			render_y += y_gap;
		}
	}

	private void renderBarChartAxes(long ctx) {
		float y_gap = bar_chart_area.w / SVCoordinate.values().length;

		float render_x = bar_chart_area.x,
				render_y = bar_chart_area.y + y_gap / 2;
		for (int i = 0; i < SVCoordinate.values().length; i++) {

			NanoVG.nvgTextAlign(ctx,
					NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_MIDDLE);
			NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nvgText(ctx, render_x, render_y + 20, min_value + "",
					MemoryUtil.NULL);

			NanoVG.nvgTextAlign(ctx,
					NanoVG.NVG_ALIGN_RIGHT | NanoVG.NVG_ALIGN_MIDDLE);
			NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nvgText(ctx, render_x + bar_chart_area.z, render_y + 20,
					max_value + "%", MemoryUtil.NULL);

			// render a white frame
			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgMoveTo(ctx, bar_chart_area.x, render_y - y_gap / 2);
			NanoVG.nvgLineTo(ctx, bar_chart_area.x + bar_chart_area.z,
					render_y - y_gap / 2);
			NanoVG.nvgLineTo(ctx, bar_chart_area.x + bar_chart_area.z,
					render_y + y_gap / 2);
			NanoVG.nvgLineTo(ctx, bar_chart_area.x, render_y + y_gap / 2);
			NanoVG.nvgClosePath(ctx);
			NanoVG.nvgStrokeWidth(ctx, 2);
			NanoVG.nvgStrokeColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nvgStroke(ctx);

			// render a horizontal line
			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgRect(ctx, render_x, render_y, bar_chart_area.z - 1, 2);
			NanoVG.nnvgFill(ctx);

			// render vertical lining labels
			float label_height = 15;
			for (int label_index = 0; label_index <= 10; label_index++) {
				NanoVG.nvgBeginPath(ctx);
				NanoVG.nvgRect(ctx, render_x - 1, render_y - label_height / 2,
						2, label_height);
				NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
				NanoVG.nvgFill(ctx);

				render_x += (bar_chart_area.z) / 10;
			}

			render_x = bar_chart_area.x;
			render_y += y_gap;
		}
	}

	private void renderSankeyDiagram(SVModel model, long ctx, float frame_width,
			float frame_height) {
		float start_x = bar_chart_area.x + bar_chart_area.z;
		float start_y = bar_chart_area.y;
		float width = frame_width - multiple_plot_area.z - bar_chart_area.z
				- 50;
		float height = multiple_plot_area.w;

		sankey_area = new Vec4(start_x, start_y, width, height);

		renderSankeyOverview(model.getLeftCity(), ViewSide.Left, ctx);
		renderSankeyOverview(model.getRightCity(), ViewSide.Right, ctx);
	}

	private void renderSankeyOverview(SVCityModel city_model, ViewSide side,
			long ctx) {
		float y_gap = sankey_area.w / 16;
		float sum_height = sankey_area.w / 4;
		float render_x = sankey_area.x + sankey_area.z;
		float render_y = sankey_area.y + sankey_area.w / 2 - y_gap - sum_height;
		if (side == ViewSide.Right)
			render_y = sankey_area.y + sankey_area.w / 2 + y_gap;

		// render a vertical line
		NanoVG.nvgBeginPath(ctx);
		NanoVG.nvgRect(ctx, render_x, render_y, 2, sum_height);
		NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(side.getColor()));
		NanoVG.nvgFill(ctx);

		// render city labels
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_MIDDLE);
		NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
		NanoVG.nvgText(ctx, render_x + 5, render_y + sum_height / 2,
				city_model.getCityConfig().getCity(), MemoryUtil.NULL);

		// render Sankey connections
		renderSankeyConnection(city_model, side, ctx, render_x, render_y,
				sum_height);
	}

	private void renderSankeyConnection(SVCityModel city_model, ViewSide side,
			long ctx, float right_x, float render_y, float sum_height) {
		float[] feature_averages = city_model.getAnalyzer().getFeatureAverage();
		float margin = 5;

		float cur_right_y = render_y;
		float cur_left_y = bar_chart_area.y
				+ bar_chart_area.w / SVCoordinate.values().length / 2;
		float left_x = bar_chart_area.x + bar_chart_area.z + margin;
		for (int i = 0; i < feature_averages.length; i++) {
			float box_h = sum_height * feature_averages[i] / 100;
			float left_y = cur_left_y - margin - box_h;
			if (side == ViewSide.Right)
				left_y = cur_left_y + margin;

			renderSankeyLine(ctx,
					ColorUtil.nvgRGB(SVCoordinate.values()[i].getColor()),
					new Vec2(left_x, left_y + box_h / 2),
					new Vec2(right_x - margin, cur_right_y + box_h / 2), box_h);

			cur_right_y += box_h;
			cur_left_y += bar_chart_area.w / SVCoordinate.values().length;
		}
	}

	private void renderSankeyLine(long ctx, NVGColor color, Vec2 left,
			Vec2 right, float sankey_width) {
		List<Vec2> line1 = SankeyLine.getSankeyLine(
				left.add(new Vec2(0, -sankey_width / 2)),
				right.add(new Vec2(0, -sankey_width / 2)), 101);

		List<Vec2> line2 = SankeyLine.getSankeyLine(
				left.add(new Vec2(0, sankey_width / 2)),
				right.add(new Vec2(0, sankey_width / 2)), 101);

		NanoVG.nvgBeginPath(ctx);
		for (int i = 0; i < line1.size(); i++) {
			Vec2 p = line1.get(i);

			if (i == 0)
				NanoVG.nvgMoveTo(ctx, p.x, p.y);
			else
				NanoVG.nvgLineTo(ctx, p.x, p.y);
		}

		for (int i = line2.size() - 1; i >= 0; i--) {
			Vec2 p = line2.get(i);

			NanoVG.nvgLineTo(ctx, p.x, p.y);
		}

		NanoVG.nvgClosePath(ctx);
		NanoVG.nvgFillColor(ctx, color);
		NanoVG.nvgFill(ctx);
	}
}
