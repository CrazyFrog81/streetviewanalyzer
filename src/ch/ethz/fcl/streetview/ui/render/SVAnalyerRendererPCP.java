/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui.render;

import java.util.List;

import org.lwjgl.nanovg.NVGColor;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryUtil;

import ch.ethz.fcl.config.UIFont;
import ch.ethz.fcl.render.NVGData;
import ch.ethz.fcl.render.RendererWithNVG;
import ch.ethz.fcl.streetview.config.SVCoordinate;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.ethz.fcl.streetview.model.SVModel;
import ch.ethz.fcl.streetview.model.gsv_result.SVSample;
import ch.ethz.fcl.streetview.ui.SVRenderer;
import ch.ethz.fcl.streetview.ui.ViewSide;
import ch.ethz.fcl.util.ColorUtil;
import ch.ethz.fcl.util.MathUtil;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec4;

public class SVAnalyerRendererPCP {
	SVRenderer render;

	Vec4 pcp_area;
	float coordinate_x_gap;
	int coord_min_value, coord_max_value;

	public SVAnalyerRendererPCP(SVRenderer render) {
		this.render = render;
	}

	public void render(SVModel sv_model, long ctx, Vec2 frame_size) {
		int width = (int) frame_size.x, height = (int) frame_size.y;
		int x = 0, y = (int) frame_size.y - height;

		coordinate_x_gap = width / (SVCoordinate.values().length);

		NanoVG.nvgBeginFrame(ctx, width, height, 1);

		// render a white frame
		NanoVG.nvgBeginPath(ctx);
		NanoVG.nvgMoveTo(ctx, 0, 0);
		NanoVG.nvgLineTo(ctx, width, 0);
		NanoVG.nvgLineTo(ctx, width, height);
		NanoVG.nvgLineTo(ctx, 0, height);
		NanoVG.nvgClosePath(ctx);
		NanoVG.nvgStrokeWidth(ctx, 3);
		NanoVG.nvgStrokeColor(ctx, RendererWithNVG.WHITE);
		NanoVG.nvgStroke(ctx);

		renderAnalyerResult(sv_model, ctx, x, y, width, height);

		try {
			renderPickedSample(sv_model.getLeftCity(), ViewSide.Left, ctx,
					(int) frame_size.x, (int) frame_size.y);
			renderPickedSample(sv_model.getRightCity(), ViewSide.Right, ctx,
					(int) frame_size.x, (int) frame_size.y);
		} catch (Exception e) {
			e.printStackTrace();
		}
		NanoVG.nvgEndFrame(ctx);
	}

	private void renderPickedSample(SVCityModel city_model, ViewSide side,
			long ctx, int frame_w, int frame_h) throws Exception {
		List<SVSample> samples = city_model.getAnalyzer().getPickSamples();
		int sample_count = 1;

		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.SMALL);
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_MIDDLE);

		for (SVSample sample : samples) {
			// render distribution information
			NanoVG.nvgBeginPath(ctx);
			float render_x = pcp_area.x;
			for (int index = 0; index < SVCoordinate.values().length; index++) {
				float value = sample
						.getResult(SVCoordinate.values()[index].getId());

				if (value > coord_max_value)
					value = coord_max_value;
				if (value < coord_min_value)
					value = coord_min_value;

				float y = MathUtil.map(value, coord_min_value, coord_max_value,
						pcp_area.y + pcp_area.w, pcp_area.y);

				if (index == 0)
					NanoVG.nvgMoveTo(ctx, render_x, y);
				else
					NanoVG.nvgLineTo(ctx, render_x, y);

				render_x += coordinate_x_gap;
			}

			NanoVG.nvgStrokeColor(ctx, ColorUtil.nvgRGB(side.getColor()));
			NanoVG.nvgStrokeWidth(ctx, 3);
			NanoVG.nvgStroke(ctx);

			// render a circle indicate the sample number
			render_x = pcp_area.x;
			for (int index = 0; index < SVCoordinate.values().length; index++) {
				float value = sample
						.getResult(SVCoordinate.values()[index].getId());

				if (value > coord_max_value)
					value = coord_max_value;
				if (value < coord_min_value)
					value = coord_min_value;

				float y = MathUtil.map(value, coord_min_value, coord_max_value,
						pcp_area.y + pcp_area.w, pcp_area.y);

				renderPickSampleNumber(city_model, side, ctx, frame_w, frame_h,
						sample_count, render_x, y);

				render_x += coordinate_x_gap;
			}

			sample_count++;
		}
	}

	private void renderPickSampleNumber(SVCityModel city_model, ViewSide side,
			long ctx, int frame_w, int frame_h, int count, float x, float y) {
		// render a circle on the picked sample position
		NanoVG.nvgBeginPath(ctx);
		NanoVG.nvgCircle(ctx, x, y, 10);
		NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
		NanoVG.nvgFill(ctx);

		// render sample number
		NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(side.getColor()));
		NanoVG.nvgText(ctx, x, y, count + "", MemoryUtil.NULL);
	}

	private void renderAnalyerResult(SVModel sv_model, long ctx, int x, int y,
			int bg_width, int bg_height) {
		float y_margin = 100;
		float start_x = x + coordinate_x_gap / 2;
		float end_x = x + bg_width - coordinate_x_gap / 2;

		pcp_area = new Vec4(start_x, y + 35, end_x - start_x,
				bg_height - y_margin - 35);

		int[][] all_distri = sv_model.getLeftCity().getAnalyzer()
				.getFeatureDistribution();
		coord_max_value = all_distri[0].length;
		coord_min_value = 0;
		
		renderCorrelation(sv_model.getLeftCity(), ViewSide.Left, ctx);
		renderCorrelation(sv_model.getRightCity(), ViewSide.Right, ctx);

		renderDistribution(sv_model.getLeftCity(), ViewSide.Left,
				ctx);
		renderDistribution(sv_model.getRightCity(), ViewSide.Right,
				ctx);

		renderAxis(ctx);
	}

	private void renderCorrelation(SVCityModel city_model, ViewSide side, long ctx) {
		List<SVSample> results = city_model.getAnalyzer().getSamples();
		int[] pcp_indices = city_model.getAnalyzer().getPCPRandomIndices();
		NVGColor color = ColorUtil.nvgRGBA(side.getColor(), 0.05f);
		float render_x = pcp_area.x;

		for (int i = 0; i < pcp_indices.length; i++) {
			SVSample result = results.get(pcp_indices[i]);
			NanoVG.nvgBeginPath(ctx);

			for (int index = 0; index < SVCoordinate.values().length; index++) {
				float value = 0;
				try {
					value = result
							.getResult(SVCoordinate.values()[index].getId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (value > coord_max_value)
					value = coord_max_value;
				if (value < coord_min_value)
					value = coord_min_value;

				float y = MathUtil.map(value, coord_min_value, coord_max_value,
						pcp_area.y + pcp_area.w, pcp_area.y);

				if (index == 0)
					NanoVG.nvgMoveTo(ctx, render_x, y);
				else
					NanoVG.nvgLineTo(ctx, render_x, y);

				render_x += coordinate_x_gap;
			}

			NanoVG.nvgClosePath(ctx);
			NanoVG.nvgStrokeColor(ctx, color);
			NanoVG.nvgStroke(ctx);

			// reposition x dimension
			render_x = pcp_area.x;
		}
	}

	// render distribution for each volume
	private void renderDistribution(SVCityModel city_model,
			ViewSide side, long ctx) {
		float render_x = pcp_area.x, render_y = pcp_area.y + pcp_area.w;

		int max_distri = city_model.getAnalyzer().getMaxDistribution();
		float max_width = coordinate_x_gap / 2 - 10;
		float unit_width = max_width / max_distri;
		float y_gap = pcp_area.w / (coord_max_value - coord_min_value);

		for (int index = 0; index < SVCoordinate.values().length; index++) {
			int[] distri = city_model.getAnalyzer()
					.getFeatureDistribution()[index];

			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgMoveTo(ctx, render_x, render_y);
			for (int i = 0; i < distri.length; i++) {
				float x = render_x - unit_width * distri[i];
				if (side == ViewSide.Right)
					x = render_x + unit_width * distri[i];

				NanoVG.nvgLineTo(ctx, x, render_y);

				render_y -= y_gap;

				NanoVG.nvgLineTo(ctx, x, render_y);
			}

			NanoVG.nvgLineTo(ctx, render_x, render_y);
			NanoVG.nvgClosePath(ctx);
			NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(side.getColor()));
			NanoVG.nvgFill(ctx);

			render_x += coordinate_x_gap;
			render_y = pcp_area.y + pcp_area.w;
		}
	}

	private void renderAxis(long ctx) {
		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.SMALL);
		NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
		float render_y = pcp_area.y;
		float render_x = pcp_area.x;

		for (int index = 0; index < SVCoordinate.values().length; index++) {
			NanoVG.nvgTextAlign(ctx,
					NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_TOP);
			NanoVG.nvgText(ctx, render_x, render_y + pcp_area.w,
					"" + coord_min_value, MemoryUtil.NULL);

			NanoVG.nvgTextAlign(ctx,
					NanoVG.NVG_ALIGN_CENTER | NanoVG.NVG_ALIGN_BOTTOM);
			NanoVG.nvgText(ctx, render_x, render_y, coord_max_value + "%",
					MemoryUtil.NULL);

			NanoVG.nvgBeginPath(ctx);
			NanoVG.nvgRect(ctx, render_x, render_y, 1, pcp_area.w);
			NanoVG.nnvgFill(ctx);
			float label_width = 15;
			for (int i = 0; i <= 10; i++) {
				NanoVG.nvgBeginPath(ctx);
				NanoVG.nvgRect(ctx, render_x - label_width / 2, render_y,
						label_width, 1);
				NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
				NanoVG.nnvgFill(ctx);

				render_y += pcp_area.w / 10;
			}

			render_x += coordinate_x_gap;
			render_y = pcp_area.y;
		}

		renderCoordinateLegends(ctx);
	}

	private void renderCoordinateLegends(long ctx) {
		int comp_w = 100, comp_h = 50;
		int margin = 15;
		float render_y = pcp_area.y + pcp_area.w + comp_h + margin;

		float render_x = pcp_area.x;

		NanoVG.nvgFontFace(ctx, NVGData.Font.LATO.getID());
		NanoVG.nvgFontSize(ctx, UIFont.MEDIUM);
		NanoVG.nvgTextAlign(ctx,
				NanoVG.NVG_ALIGN_MIDDLE | NanoVG.NVG_ALIGN_CENTER);
		for (SVCoordinate comp : SVCoordinate.values()) {
			NanoVG.nnvgBeginPath(ctx);
			NanoVG.nvgRect(ctx, render_x - comp_w / 2, render_y - comp_h / 2,
					comp_w, comp_h);
			NanoVG.nvgFillColor(ctx, ColorUtil.nvgRGB(comp.getColor()));
			NanoVG.nvgFill(ctx);

			NanoVG.nvgFillColor(ctx, RendererWithNVG.WHITE);
			NanoVG.nvgText(ctx, render_x, render_y, comp.getId(),
					MemoryUtil.NULL);

			render_x += coordinate_x_gap;
		}
	}
}
