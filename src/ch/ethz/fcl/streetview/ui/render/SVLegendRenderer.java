/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui.render;

import java.nio.IntBuffer;

import org.lwjgl.nanovg.NVGPaint;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import ch.ethz.fcl.config.UIFont;
import ch.ethz.fcl.render.NVGData;
import ch.ethz.fcl.render.RendererWithNVG;
import ch.fhnw.util.math.Vec2;

public class SVLegendRenderer {
	private NVGPaint paint = NVGPaint.create();

	public SVLegendRenderer() {
	}

	public void render(long nvgContext, NVGData data, Vec2 frame_size) {
		NanoVG.nvgBeginFrame(nvgContext, (int) frame_size.x, (int) frame_size.y,
				1);
		renderProjectTitle(nvgContext, 80, 80);
		renderCIVALInfo(nvgContext, data, (int) frame_size.x - 480, 80, 400,
				200);
		NanoVG.nvgEndFrame(nvgContext);
	}

	private void renderProjectTitle(long nvgContext, int x, int y) {
		String title = "A Hierarchical Visual Exploration";
		String title2 = "of Human-Scale Factors in Cities";

		NanoVG.nvgFontFace(nvgContext, NVGData.Font.ADELLE_PE_BOLD.getID());
		NanoVG.nvgFontSize(nvgContext, UIFont.LARGE);

		NanoVG.nvgTextAlign(nvgContext,
				NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_MIDDLE);
		NanoVG.nvgFillColor(nvgContext, RendererWithNVG.WHITE);

		NanoVG.nvgText(nvgContext, x, y, title, MemoryUtil.NULL);
		NanoVG.nvgText(nvgContext, x, y + UIFont.LARGE, title2,
				MemoryUtil.NULL);
	}

	private void renderCIVALInfo(long nvgContext, NVGData data, int x, int y,
			int bg_width, int bg_height) {
		int render_x = x, render_y = y;
		// render FCL logo
		int img_width = bg_width;
		MemoryStack stack = MemoryStack.stackPush();
		IntBuffer imgw = stack.mallocInt(1), imgh = stack.mallocInt(1);
		NanoVG.nvgImageSize(nvgContext, data.fcl_image, imgw, imgh);
		float img_ratio = imgw.get(0) * 1.0f / imgh.get(0);

		NanoVG.nvgImagePattern(nvgContext, render_x, render_y, img_width,
				img_width / img_ratio, 0, data.fcl_image, 1, paint);
		NanoVG.nvgBeginPath(nvgContext);
		NanoVG.nvgRect(nvgContext, render_x, render_y, img_width,
				img_width / img_ratio);
		NanoVG.nvgFillPaint(nvgContext, paint);
		NanoVG.nvgFill(nvgContext);

		// render CIVAL information
		render_y += img_width / img_ratio + UIFont.MEDIUM;
		NanoVG.nvgFontFace(nvgContext, NVGData.Font.ADELLE_PE_ITALIC.getID());
		NanoVG.nvgFontSize(nvgContext, UIFont.MEDIUM);
		NanoVG.nvgFillColor(nvgContext, RendererWithNVG.WHITE);
		NanoVG.nvgTextAlign(nvgContext,
				NanoVG.NVG_ALIGN_RIGHT | NanoVG.NVG_ALIGN_MIDDLE);
		NanoVG.nvgText(nvgContext, render_x + bg_width, render_y,
				"developed by CIVAL", MemoryUtil.NULL);
	}
}
