/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui;

import java.io.File;
import java.io.IOException;

import ch.ethz.fcl.render.NVGData;
import ch.ethz.fcl.render.RendererWithNVG;
import ch.ethz.fcl.streetview.config.SVGeneralConfig;
import ch.ethz.fcl.streetview.model.SVModel;
import ch.ethz.fcl.streetview.ui.render.SVAnalyerRendererScatterPlot;
import ch.ethz.fcl.streetview.ui.render.SVLegendRenderer;
import ch.ethz.fcl.streetview.ui.render.SVMapRenderer;
import ch.ethz.fcl.streetview.ui.render.SVPreviewRenderer;
import ch.fhnw.ether.image.IGPUImage;

public class SVRenderer extends RendererWithNVG {
	SVModel model;
	SVUI ui;

	SVLegendRenderer legend_render;
	SVAnalyerRendererScatterPlot info_render;
	SVMapRenderer map_render;
	SVPreviewRenderer preview_render;

	public static IGPUImage CLOSE_BUTTON_IMG = null;

	public SVRenderer(SVModel model, SVUI ui) {
		super();

		this.model = model;
		this.ui = ui;

		this.legend_render = new SVLegendRenderer();
		this.info_render = new SVAnalyerRendererScatterPlot(this);
		this.map_render = new SVMapRenderer(this);
		this.preview_render = new SVPreviewRenderer(this);

		try {
			CLOSE_BUTTON_IMG = IGPUImage.read(
					new File("src/resources/icons/close.png").toURI().toURL());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void renderNVG(IRenderTargetState state, NVGData data) {
		super.setup(state);

		if (!SVGeneralConfig.GENERATE_SAMPLE_LOCATIONS) {
			String title = state.getView().getWindow().getTitle();

			if (title.contains("Left")) {
				map_render.render(state.getView(), model.getLeftCity(),
						ViewSide.Left, nvgContext, frame_size);
			} else if (title.contains("Right"))
				map_render.render(state.getView(), model.getRightCity(),
						ViewSide.Right, nvgContext, frame_size);
			else if (title.contains("Bottom")) {
				info_render.render(model, nvgContext, frame_size);
			} else {
				preview_render.render(model, nvgContext, frame_size);
			}
		}
	}
}
