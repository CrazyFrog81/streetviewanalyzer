/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui.interaction;

import java.util.List;

import ch.ethz.fcl.streetview.config.SVGeneralConfig;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.ethz.fcl.streetview.model.SVModel;
import ch.ethz.fcl.streetview.model.gsv_result.SVSample;
import ch.ethz.fcl.streetview.ui.SVUI;
import ch.ethz.fcl.streetview.ui.ViewSide;
import ch.ethz.fcl.util.ProjectionUtil;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.IKeyEvent;
import ch.fhnw.ether.controller.event.IPointerEvent;
import ch.fhnw.ether.controller.tool.AbstractTool;
import ch.fhnw.ether.view.IView;
import ch.fhnw.util.math.Vec3;

public class SVInteractionTool extends AbstractTool {
	private IController controller;
	private SVModel model;
	protected SVUI ui;

	private SVSample selected_sample = null;
	private float mouseX;
	private float mouseY;

	private boolean refresh_scene = false;

	protected SVInteractionTool(IController controller) {
		super(controller);

		this.controller = controller;
	}

	public SVInteractionTool(IController controller, SVModel model, SVUI ui) {
		this(controller);

		this.model = model;
		this.ui = ui;
	}

	@Override
	public void keyPressed(IKeyEvent e) {

	}

	@Override
	public void refresh(IView view) {
		String title = view.getWindow().getTitle();
		if (title.contains("Right") || title.contains("Left")) {
			SVCityModel city_model = model.getLeftCity();
			if (view.getWindow().getTitle().contains("Right"))
				city_model = model.getRightCity();

			List<SVSample> samples = city_model.getAnalyzer().getPickSamples();
			for (SVSample sample : samples) {
				sample.scaleMeshes(controller, city_model);
			}
			refresh_scene = true;

			view.getController().getCamera(view).getUpdater().request();

			// switch between detailed and overview meshes
			float camera_dist = view.getController().getCamera(view)
					.getPosition().z;

//			System.out.println("camera dist " + camera_dist);

			if (Math.abs(camera_dist) <= 0.1f)
				city_model.showDetailedSampleMesh(
						view.getController().getScene());
			else
				city_model.showOverviewSampleMesh(
						view.getController().getScene());

			city_model.updateScaleWorldDist(view);
		}

		// System.out.println(
		// view.getController().getCamera(view).getPosition().toString());
	}

	@Override
	public void pointerMoved(IPointerEvent e) {
		if (refresh_scene) {
			SVCityModel city_model = model.getLeftCity();
			if (e.getView().getWindow().getTitle().contains("Right"))
				city_model = model.getRightCity();

			List<SVSample> samples = city_model.getAnalyzer().getPickSamples();
			for (SVSample sample : samples) {
				sample.scaleMeshes(controller, city_model);
			}
			refresh_scene = false;
		}
	}

	@Override
	public void pointerPressed(IPointerEvent e) {
		String title = e.getView().getWindow().getTitle();

		// click on the preview
		if (title.contains("Comparison")) {
			float width = e.getView().getViewport().w;
			int index = (int) (e.getX()
					/ (width / model.getCityModels().length));
			model.selected_city = model.getCityModels()[index];
		}
		// click on the left or right city view
		else if (title.contains("Left") || title.contains("Right")) {
			Vec3 p = ProjectionUtil.getWorldCoord(e.getView(), e.getX(),
					e.getY());

			SVCityModel city_model = model.getLeftCity();
			if (e.getView().getWindow().getTitle().contains("Right"))
				city_model = model.getRightCity();

			if (SVGeneralConfig.GENERATE_SAMPLE_LOCATIONS)
				city_model.getOSM().findNearestNode(controller, p.x, p.y);
			else {
				selected_sample = null;
				List<SVSample> samples = city_model.getAnalyzer()
						.getPickSamples();

				for (SVSample sample : samples) {
					if (sample.clickOnClose(p.x, p.y)) {
						sample.removeMeshes(controller);
						samples.remove(sample);
						return;
					}
				}

				for (SVSample sample : samples) {
					if (sample.clickOnSampleFrame(e.getX(), e.getY())) {
						selected_sample = sample;
						break;
					}
				}

				if (selected_sample == null)
					city_model.getAnalyzer().pickResult(controller, p.x, p.y);
			}

			mouseX = e.getX();
			mouseY = e.getY();
		}
	}

	@Override
	public void pointerDragged(IPointerEvent e) {
		if (selected_sample != null) {
			float dx = e.getX() - mouseX;
			float dy = e.getY() - mouseY;

			if (e.getView().getWindow().getTitle().contains("Left"))
				selected_sample.moveMeshes(controller, model.getLeftCity(), dx,
						dy);
			else if (e.getView().getWindow().getTitle().contains("Right"))
				selected_sample.moveMeshes(controller, model.getRightCity(), dx,
						dy);

			mouseX = e.getX();
			mouseY = e.getY();
		}
	}

	@Override
	public void pointerReleased(IPointerEvent e) {
		if (model.selected_city != null) {
			if (e.getY() < 0) { // dragged to the left or right city view
				float width = e.getView().getViewport().w;
				if (e.getX() < width / 2 && e.getX() >= 0) {
					model.changeCity(e.getView().getController().getScene(),
							e.getView().getController().getViews().get(1),
							ViewSide.Left, model.selected_city);

					refresh(e.getView().getController().getViews().get(1));
				} else if (e.getX() >= width / 2 && e.getX() < width) {
					model.changeCity(e.getView().getController().getScene(),
							e.getView().getController().getViews().get(2),
							ViewSide.Right, model.selected_city);

					refresh(e.getView().getController().getViews().get(2));
				}
			}
		}

		model.selected_city = null;
	}
}
