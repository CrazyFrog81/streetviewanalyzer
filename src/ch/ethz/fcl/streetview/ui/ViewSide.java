/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui;

import ch.fhnw.util.color.RGB;

public enum ViewSide {
	Left(216 / 255.0f, 179 / 255.0f, 101 / 255.0f), Right(90 / 255.0f,
			180 / 255.0f, 172 / 255.0f);

	ViewSide(float r, float g, float b) {
		this.color = new RGB(r, g, b);
	}

	public RGB getColor() {
		return color;
	}

	private RGB color;
}
