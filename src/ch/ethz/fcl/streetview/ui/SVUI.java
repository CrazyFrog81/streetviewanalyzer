/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.ui;

import java.io.IOException;

import ch.ethz.fcl.streetview.model.SVModel;
import ch.ethz.fcl.streetview.ui.interaction.SVInteractionTool;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.tool.ITool;
import ch.fhnw.ether.controller.tool.NavigationTool;

public class SVUI {
	private IController controller;

	private SVModel model;
	private SVInteractionTool interaction_tool;

	public SVUI(SVModel model) {
		this.model = model;
	}

	public void enable(IController controller) throws IOException {
		this.controller = controller;
		model.addToScene(controller.getScene());

		interaction_tool = new SVInteractionTool(controller, model, this);
		ITool naviTool = new NavigationTool(controller, interaction_tool);

		controller.setTool(naviTool);
	}

	public IController getController() {
		return controller;
	}
}
