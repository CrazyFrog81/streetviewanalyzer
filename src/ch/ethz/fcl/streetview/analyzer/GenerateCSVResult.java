package ch.ethz.fcl.streetview.analyzer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class GenerateCSVResult {
	public static void main(String[] args){
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(new File(Utils.IMAGE_LINK_FILE)));
			BufferedWriter csv_bw = new BufferedWriter(new FileWriter(new File(Utils.PARENT_FOLDER+"results.csv")));
			csv_bw.write("index, latitude, longitude, green, sky, others\n");
			
			String line = br.readLine();
			int count = 0;
			while(line != null){
				String[] all_info = line.split(":");
				
				// read the lat lng
				String[] image_info = all_info[2].split("&");
				String latlng = image_info[2].replace("location=", "");
				double lat = Double.parseDouble(latlng.split(",")[0]);
				double lng = Double.parseDouble(latlng.split(",")[1]);
				
				// read the value of green, sky and Others
				String result_file = Utils.IMAGE_SAVE_FOLDER+all_info[0]+".txt";
				float green = 0, sky = 0, others = 0;
				@SuppressWarnings("resource")
				BufferedReader result_br = new BufferedReader(new FileReader(new File(result_file)));
				String result_line = result_br.readLine();
				while(result_line != null){
					String[] results = result_line.split(":");
					String id = results[0];
					float value = Float.parseFloat(results[1].replace("%", ""));
					if(id.contains("Sky"))
						sky = value;
					else if(id.contains("Tree"))
						green = value;
					else
						others += value;
					
					result_line = result_br.readLine();
				}
				
				csv_bw.write(count+", "+lat +", "+lng+", "+green+", "+sky +", "+others+"\n" );
				
				count++;
				line = br.readLine();
			}
			
			csv_bw.close();
			System.out.println("finished generating result csv file");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
