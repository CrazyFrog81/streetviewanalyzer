package ch.ethz.fcl.streetview.analyzer.caffe;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;

import javax.imageio.ImageIO;

import ch.ethz.fcl.streetview.analyzer.Utils;

public class AnalyzeCaffeResults {
	public static void main(String[] args) {
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(new File(Utils.CAFFE_INPUT_LIST)));

			CaffeColor[] all_colors = CaffeColor.values();
			HashMap<String, Integer> color_id_index_map = new HashMap<String, Integer>();

			for (int i = 0; i < all_colors.length; i++) {
				Color color = all_colors[i].getCaffeColor();
				color_id_index_map.put(color.getRed() + "_" + color.getGreen() + "_" + color.getBlue(), i);
			}
			
			String line = br.readLine();
			int count = 0;
			while (line != null) {
				String input_image_file = line.split(" ")[0];
				String classification_image_file = Utils.CAFFE_CLASSIFICATION_FOLDER + count + "_result_segnet.png";
				BufferedImage classification_image = ImageIO.read(new File(classification_image_file));
											
				int[] colors_pixel_count = new int[all_colors.length];

				for (int x = 0; x < classification_image.getWidth(); x++) {
					for (int y = 0; y < classification_image.getHeight(); y++) {
						int clr = classification_image.getRGB(x, y);
						
						int red = (clr & 0x00ff0000) >> 16;
						int green = (clr & 0x0000ff00) >> 8;
						int blue = clr & 0x000000ff;
												
						String id = red + "_" + green + "_" + blue;
						
						Integer index = color_id_index_map.get(id);

						if (index != null) {
							colors_pixel_count[index]++;
						}
					}
				}

				ImageIO.write(classification_image, "png", new File(input_image_file.replace(".jpeg", ".png")));
				
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File(input_image_file.replace(".jpeg", ".txt"))));
				float sum = 0;
				for (int i = 0; i < all_colors.length; i++) {
					float percentage = colors_pixel_count[i] * 100.0f
							/ (classification_image.getWidth() * classification_image.getHeight());
					sum += percentage;
					bw.write(all_colors[i].getName() + ": "+ percentage+"%\n");
				}

				bw.close();
				System.out.println(classification_image_file+" "+count + " " + input_image_file +" "+sum);

				count++;
				line = br.readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
