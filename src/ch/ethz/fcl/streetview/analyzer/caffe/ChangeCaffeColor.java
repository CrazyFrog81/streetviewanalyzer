package ch.ethz.fcl.streetview.analyzer.caffe;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class ChangeCaffeColor {
	public static void main(String[] args) {
		try {
			CaffeColor[] old_colors = CaffeColor.values();
			HashMap<String, Integer> color_id_index_map = new HashMap<String, Integer>();

			for (int i = 0; i < old_colors.length; i++) {
				Color color = old_colors[i].getUIColor();
				color_id_index_map.put(color.getRed() + "_" + color.getGreen()
						+ "_" + color.getBlue(), i);
			}

			BufferedImage classification_image = ImageIO
					.read(new File("img196_recolor.png"));

			String recolor_file = "img_196.png";

			BufferedImage recolor_image = new BufferedImage(
					classification_image.getWidth(),
					classification_image.getHeight(),
					classification_image.getType());

			for (int x = 0; x < classification_image.getWidth(); x++) {
				for (int y = 0; y < classification_image.getHeight(); y++) {
					int clr = classification_image.getRGB(x, y);

					int red = (clr & 0x00ff0000) >> 16;
					int green = (clr & 0x0000ff00) >> 8;
					int blue = clr & 0x000000ff;

					String id = red + "_" + green + "_" + blue;

					Integer index = color_id_index_map.get(id);

					if (index != null) {
						Color new_color = CaffeColorNew.values()[index].getColor();
						int new_rgb = new_color.getRGB();
						recolor_image.setRGB(x, y, new_rgb);
					}
				}
			}

			ImageIO.write(recolor_image, "png", new File(recolor_file));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
