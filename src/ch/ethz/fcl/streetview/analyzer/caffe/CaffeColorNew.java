/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.analyzer.caffe;

import java.awt.Color;

public enum CaffeColorNew {
	// @formatter:off
		Sky("Sky", new Color(128,203,215)),
		Building("Building", new Color(248,177,118)),
		Pole("Pole", new Color(219,216,215)),
		Road_marking("Road_marking", new Color(219,216,215)),
		Road("Road", new Color(185,154,147)),
		Pavement("Pavement", new Color(219,216,215)),
		Tree("Tree", new Color(130,194,131)),
		SignSymbol("SignSymbol", new Color(219,216,215)),
		Fence("Fence", new Color(219,216,215)),
		Car("Car", new Color(247,193,193)),
		Pedestrian("Pedestrian", new Color(219,216,215)),
		Bicyclist("Bicyclist", new Color(219,216,215)),
		Unlabelled("Unlabelled", new Color(219,216,215));
		// @formatter:on

		private String name;
		private Color color;

		CaffeColorNew(String name, Color color) {
			this.name = name;
			this.color = color;
		}

		public Color getColor() {
			return color;
		}

		public String getName() {
			return name;
		}
}
