package ch.ethz.fcl.streetview.analyzer.caffe;

import java.awt.Color;

public enum CaffeColor {
	// @formatter:off
	Sky("Sky", new Color(128,128,128), new Color(55, 126, 184)),
	Building("Building", new Color(128,0,0), new Color(240, 2, 127)),
	Pole("Pole", new Color(192,192,128), new Color(102, 102, 102)),
	Road_marking("Road_marking", new Color(255,69,0), new Color(190, 174, 212)),
	Road("Road", new Color(128,64,128), new Color(190, 174, 212)),
	Pavement("Pavement", new Color(60,40,222), new Color(102, 102, 102)),
	Tree("Tree", new Color(128,128,0), new Color(77, 175, 74)),
	SignSymbol("SignSymbol", new Color(192,128,128), new Color(102, 102, 102)),
	Fence("Fence", new Color(64,64,128), new Color(102, 102, 102)),
	Car("Car", new Color(64,0,128), new Color(191, 91, 23)),
	Pedestrian("Pedestrian", new Color(64,64,0), new Color(102, 102, 102)),
	Bicyclist("Bicyclist", new Color(0,128,192), new Color(102, 102, 102)),
	Unlabelled("Unlabelled", new Color(0,0,0), new Color(102, 102, 102));
	// @formatter:on

	private String name;
	private Color caffe_color;
	private Color ui_color;

	CaffeColor(String name, Color caffe_color, Color ui_color) {
		this.name = name;
		this.caffe_color = caffe_color;
		this.ui_color = ui_color;
	}

	public Color getCaffeColor() {
		return caffe_color;
	}
	
	public Color getUIColor(){
		return ui_color;
	}

	public String getName() {
		return name;
	}
}
