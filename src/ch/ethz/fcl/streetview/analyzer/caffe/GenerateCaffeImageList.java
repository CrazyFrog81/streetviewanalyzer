package ch.ethz.fcl.streetview.analyzer.caffe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import ch.ethz.fcl.streetview.analyzer.Utils;

public class GenerateCaffeImageList {
	public static void main(String[] args) {
		try {
			File file = new File(Utils.CAFFE_INPUT_LIST);

			if (!file.exists()) {
				file.createNewFile();
			}

			BufferedWriter bw = new BufferedWriter(new FileWriter(file));

			@SuppressWarnings("resource")
			Stream<Path> paths = Files.walk(Paths.get(Utils.CAFEE_INPUT_FOLDER));

			paths.forEach(filePath -> {
				if (Files.isRegularFile(filePath)) {
					try {
						bw.write(filePath.toString() + " " + filePath.toString() + "\n");

						// open an image
						// Runtime.getRuntime().exec("eog filename");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			bw.close();

			System.out.println("finished writting images");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
