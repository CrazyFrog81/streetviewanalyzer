package ch.ethz.fcl.streetview.analyzer;

public class Utils {
	public static final String API_KEY = "&key=AIzaSyB8nONTMzbohZGrQlWJvC0ez4bF0_IUlRk"; // Zeng Wei
//	public static final String API_KEY = "&key=AIzaSyANmhcAYqNs3SfWXtWcwHywp_qJc87V8GE"; // FCL CIVAL
//	public static final String API_KEY = "&key=AIzaSyBuWJzBhPgmbdA1yRKlAIVE9PtgneW2MCk"; // Crazy Frog

	public static final String IMAGE_SAVE_FOLDER = "res/";
	// Singapore
//	public static final String PARENT_FOLDER = "res/Singapore/";
//	public static final String IMAGE_LINK_FILE = "res/Singapore/singapore.image_list.txt";
//	public static final String CAFEE_INPUT_FOLDER = "/home/cival/Documents/workspace/StreetView/res/Singapore/images"; 
//	public static final String CAFFE_INPUT_LIST = "/home/cival/Documents/workspace/StreetView/res/Singapore/singapore_caffe.txt";
//	public static final String CAFFE_CLASSIFICATION_FOLDER = "/home/cival/Documents/workspace/StreetView/res/Singapore/classification/";
	
	// HongKong
	public static final String PARENT_FOLDER = "res/HongKong/";
	public static final String IMAGE_LINK_FILE = "res/HongKong/hongkong.image_list.txt"; 
	public static final String CAFEE_INPUT_FOLDER = "/home/cival/Documents/workspace/StreetView/res/HongKong/images"; 
	public static final String CAFFE_INPUT_LIST = "/home/cival/Documents/workspace/StreetView/res/HongKong/hongkong_caffe.txt";
	public static final String CAFFE_CLASSIFICATION_FOLDER = "/home/cival/Documents/workspace/StreetView/res/HongKong/classification/";
}