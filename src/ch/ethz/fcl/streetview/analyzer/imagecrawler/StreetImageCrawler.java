/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.analyzer.imagecrawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import ch.ethz.fcl.streetview.analyzer.Utils;

public class StreetImageCrawler {
	public StreetImageCrawler() {
		load();
	}

	private void load() {		
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(Utils.IMAGE_LINK_FILE));

			String line = br.readLine();
			int line_count = 1;
			int skipped_count = 0;

			while (line_count < 100 && line != null && line != "") {
				String[] img_info = line.split(":");

				String file_path = img_info[0];
				String img_link = img_info[1] + ":" + img_info[2] + Utils.API_KEY;
				
				img_link = img_link.replace("640x640", "480x360");
				
				System.out.println("Image link: " + img_link);

				line = br.readLine();
				
				String folder = Utils.IMAGE_SAVE_FOLDER + file_path.split("img")[0];
				new File(folder).mkdir();

				File img_file = new File(Utils.IMAGE_SAVE_FOLDER + file_path + ".jpeg");
				if (img_file.exists()) {
					skipped_count++;
					System.out.println("Skip image count: " + skipped_count+" @ "+file_path);
					continue;
				}

				try {
					ImageIO.write(ImageIO.read(new URL(img_link)), "jpeg", img_file);
				} catch (IIOException e) {
					continue;
				}

				System.out.println("Saved new image " + line_count + " @ "+file_path);

				line_count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new StreetImageCrawler();
	}
}
