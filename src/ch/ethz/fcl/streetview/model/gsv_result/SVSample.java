/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.model.gsv_result;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import ch.ethz.fcl.streetview.config.SVGeneralConfig;
import ch.ethz.fcl.streetview.config.SVCoordinate;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.ethz.fcl.streetview.ui.SVRenderer;
import ch.ethz.fcl.util.ImageUtil;
import ch.ethz.fcl.util.ProjectionUtil;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.image.IGPUImage;
import ch.fhnw.ether.scene.camera.IViewCameraState;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.IMesh.Primitive;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry;
import ch.fhnw.ether.scene.mesh.material.ColorMaterial;
import ch.fhnw.ether.view.ProjectionUtilities;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Mat4;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.Vec4;

public class SVSample {
	String image_link;

	Vec2 pos; // position in lat and lng
	float[] results;

	float[] ordered_results;
	int[] ordered_index;

	private IGPUImage raw_img = null, classification_img = null;
	protected IMesh raw_mesh = null, classification_mesh = null;
	protected IMesh close_mesh = null;
	protected IMesh frame_mesh = null;
	protected IMesh connection_mesh = null;

	public float frame_screen_width = 240, frame_screen_height;
	public Vec3 sample_screen_pos = null, frame_screen_pos;

	public Vec4 close_pos = null;

	public SVSample(String image_link, float lat, float lng) {
		this.image_link = image_link;
		this.pos = new Vec2(lng, lat);
		results = new float[SVCoordinate.values().length];
	}

	public String getRawImageLink() {
		return SVGeneralConfig.DATA_FOLDER + image_link + ".jpeg";
	}

	public String getResultImageLink() {
		return SVGeneralConfig.DATA_FOLDER + image_link + "_recolor.png";
	}

	public Vec2 getPos() {
		return pos;
	}

	public float getLng() {
		return pos.x;
	}

	public float getLat() {
		return pos.y;
	}

	public void setValue(int index, float value) throws Exception {
		if (index < 0 || index >= results.length)
			throw new Exception("result index out of bound");

		if (value < 0 || value > 100)
			throw new Exception("result value out of bound [0 - 100]");

		results[index] = value;
	}

	public void initMeshes(IController controller, SVCityModel model) {
		updateScreenPos(controller, model);
		addMeshes(controller, model);
	}

	private void updateScreenPos(IController controller, SVCityModel model) {
		float x = model.lngToX(getLng());
		float y = model.latToY(getLat());
		float z = 0.00002f;
		IViewCameraState state = controller.getRenderManager()
				.getViewCameraState(controller.getCurrentView());

		float img_raw_w = 480, img_raw_h = 360;
		frame_screen_height = img_raw_h * frame_screen_width / img_raw_w;

		float frame_screen_dx = 0, frame_screen_dy = frame_screen_height / 3;
		if (frame_screen_pos != null) {
			frame_screen_dx = frame_screen_pos.x - sample_screen_pos.x;
			frame_screen_dy = frame_screen_pos.y - sample_screen_pos.y;
		}

		sample_screen_pos = ProjectionUtilities.projectToScreen(state,
				new Vec3(x, y, z));
		frame_screen_pos = new Vec3(sample_screen_pos.x + frame_screen_dx,
				sample_screen_pos.y + frame_screen_dy, sample_screen_pos.z);
	}

	private void addMeshes(IController controller, SVCityModel model) {
		float z = 0.0002f;
		float x = model.lngToX(getLng());
		float y = model.latToY(getLat());

		Vec3 frame_world_pos = ProjectionUtil.getWorldCoord(
				controller.getCurrentView(), frame_screen_pos.x,
				frame_screen_pos.y);
		float img_world_w = ProjectionUtil.getSceneDistance(
				controller.getCurrentView(), frame_screen_width),
				img_world_h = ProjectionUtil.getSceneDistance(
						controller.getCurrentView(), frame_screen_height);

		String raw_image_link = getRawImageLink();
		String result_image_link = getResultImageLink();
		if (raw_img == null)
			try {
				raw_img = IGPUImage
						.read(new File(raw_image_link).toURI().toURL());
				classification_img = IGPUImage
						.read(new File(result_image_link).toURI().toURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		raw_mesh = ImageUtil.makeTexture(raw_img,
				frame_world_pos.x - img_world_w, frame_world_pos.y, z,
				img_world_w, img_world_h);
		classification_mesh = ImageUtil.makeTexture(classification_img,
				frame_world_pos.x, frame_world_pos.y, z, img_world_w,
				img_world_h);

		//@formatter:off
		float[] frame_vertices = new float[] { 
				frame_world_pos.x - img_world_w, frame_world_pos.y, z, 
				frame_world_pos.x + img_world_w, frame_world_pos.y, z, 
				frame_world_pos.x - img_world_w, frame_world_pos.y + img_world_h, z,
				frame_world_pos.x + img_world_w, frame_world_pos.y + img_world_h, z,
				frame_world_pos.x - img_world_w, frame_world_pos.y, z, 
				frame_world_pos.x - img_world_w, frame_world_pos.y + img_world_h, z, 
				frame_world_pos.x + img_world_w, frame_world_pos.y, z, 
				frame_world_pos.x + img_world_w, frame_world_pos.y + img_world_h, z, 
				frame_world_pos.x, frame_world_pos.y, z, 
				frame_world_pos.x, frame_world_pos.y + img_world_h, z, 
		};
		//@formater:on
		IGeometry g = DefaultGeometry.createV(frame_vertices);
		frame_mesh = new DefaultMesh(Primitive.LINES, new ColorMaterial(RGBA.WHITE), g);
		
		float[] connection_vertices = {
				frame_world_pos.x, frame_world_pos.y, z, 
				x, y, z, 
		};
		g = DefaultGeometry.createV(connection_vertices);
		connection_mesh = new DefaultMesh(Primitive.LINES, new ColorMaterial(RGBA.WHITE), g);
		
		float close_width = ProjectionUtil
				.getSceneDistance(controller.getCurrentView(), 30);
		close_pos = new Vec4(frame_world_pos.x + img_world_w - close_width,
				frame_world_pos.y + img_world_h - close_width, close_width,
				close_width);
		close_mesh = ImageUtil.makeTexture(SVRenderer.CLOSE_BUTTON_IMG,
				close_pos.x, close_pos.y, z * 10f, close_pos.w, close_pos.z);

		controller.getScene().add3DObject(close_mesh);
		controller.getScene().add3DObject(raw_mesh);
		controller.getScene().add3DObject(classification_mesh);
		controller.getScene().add3DObject(frame_mesh);
		controller.getScene().add3DObject(connection_mesh);
	}

	public boolean clickOnSampleFrame(float x, float y) {
		return x >= frame_screen_pos.x - frame_screen_width
				&& x <= frame_screen_pos.x + frame_screen_width
				&& y >= frame_screen_pos.y
				&& y <= frame_screen_pos.y + frame_screen_height;
	}

	public boolean clickOnClose(float world_x, float world_y) {
		return world_x >= close_pos.x && world_x <= close_pos.x + close_pos.z
				&& world_y >= close_pos.y
				&& world_y <= close_pos.y + close_pos.w;
	}

	public void removeMeshes(IController controller) {
		controller.getScene().remove3DObject(classification_mesh);
		controller.getScene().remove3DObject(raw_mesh);
		controller.getScene().remove3DObject(close_mesh);
		controller.getScene().remove3DObject(frame_mesh);
		controller.getScene().remove3DObject(connection_mesh);
	}

	public void moveMeshes(IController controller, SVCityModel city_model, float dx,
			float dy) {
		Vec3 old_world_pos = ProjectionUtil.getWorldCoord(
				controller.getCurrentView(), frame_screen_pos.x,
				frame_screen_pos.y);

		frame_screen_pos = new Vec3(frame_screen_pos.x + dx,
				frame_screen_pos.y + dy, frame_screen_pos.z);

		Vec3 new_world_pos = ProjectionUtil.getWorldCoord(
				controller.getCurrentView(), frame_screen_pos.x,
				frame_screen_pos.y);

		float close_width = ProjectionUtil
				.getSceneDistance(controller.getCurrentView(), 30);
		float img_world_w = ProjectionUtil.getSceneDistance(
				controller.getCurrentView(), frame_screen_width),
				img_world_h = ProjectionUtil.getSceneDistance(
						controller.getCurrentView(), frame_screen_height);
		close_pos = new Vec4(new_world_pos.x + img_world_w - close_width,
				new_world_pos.y + img_world_h - close_width, close_width,
				close_width);

		Mat4 translate = Mat4.translate(new_world_pos.x - old_world_pos.x,
				new_world_pos.y - old_world_pos.y, 0);

		Mat4 transform = Mat4.multiply(translate, raw_mesh.getTransform());

		raw_mesh.setTransform(transform);
		classification_mesh.setTransform(transform);
		close_mesh.setTransform(transform);
		frame_mesh.setTransform(transform);

		controller.getScene().remove3DObject(connection_mesh);
		float z = 0.0002f;
		float x = city_model.lngToX(getLng());
		float y = city_model.latToY(getLat());

		Vec3 frame_world_pos = ProjectionUtil.getWorldCoord(
				controller.getCurrentView(), frame_screen_pos.x,
				frame_screen_pos.y);
		float[] connection_vertices = {
				frame_world_pos.x, frame_world_pos.y, z, 
				x, y, z, 
		};
		IGeometry g = DefaultGeometry.createV(connection_vertices);
		connection_mesh = new DefaultMesh(Primitive.LINES, new ColorMaterial(RGBA.WHITE), g);
		controller.getScene().add3DObject(connection_mesh);
	}

	public void scaleMeshes(IController controller, SVCityModel model) {
		removeMeshes(controller);
		updateScreenPos(controller, model);
		addMeshes(controller, model);
	}

	public float[] getResults() {
		return results;
	}

	public void orderResults() {
		ordered_results = new float[results.length];
		ordered_index = new int[results.length];

		for (int i = 0; i < results.length; i++) {
			float max_value = -1;
			int order_index = -1;
			for (int j = 0; j < results.length; j++) {
				boolean ordered = false;
				for (int k = 0; k < i; k++) {
					if (ordered_index[k] == j) {
						ordered = true;
						break;
					}
				}
				if (ordered)
					continue;

				if (results[j] > max_value) {
					order_index = j;
					max_value = results[j];
				}
			}

			if (order_index == -1)
				System.out.println(max_value + " " + i);

			ordered_index[i] = order_index;
			ordered_results[i] = max_value;
		}
	}

	public float[] getOrderedResults() {
		return ordered_results;
	}

	public int[] getOrderedIndex() {
		return ordered_index;
	}

	public float getResult(String id) throws Exception {
		for (SVCoordinate com : SVCoordinate.values()) {
			if (com.getId().equals(id))
				return results[com.getIndexInResultFile() - SVCoordinate.START_INDEX];
		}

		throw new Exception("Result for the id " + id + " not found");
	}
}
