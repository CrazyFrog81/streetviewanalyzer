/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.model.gsv_result;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.ethz.fcl.algorithm.treemap.TreeMap;
import ch.ethz.fcl.streetview.config.SVCoordinate;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.ethz.fcl.util.CoordUtil;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.IMesh.Primitive;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.material.ColorMaterial;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Vec4;

public class SVAnalyzer {
	SVCityModel city_model = null;

	private IMesh detailed_sample_mesh; // tree map showing the percentages of
										// all coordinates
	private IMesh overview_sample_mesh; // rectangles showing only the highest
										// percentage of a sample
	private float mesh_size = 0.001f;

	private List<SVSample> samples;
	private int[][] feature_distribution; // distribution counts of features in
											// every 1%

	private float[] feature_averages;
	private int distri_size = 50;
	private int max_distribution = -1;

	private int[] pcp_random_indices = null;

	List<SVSample> pick_samples;

	public SVAnalyzer(SVCityModel model) {
		this.city_model = model;
		pick_samples = new ArrayList<SVSample>();
	}

	public void load() {
		samples = new ArrayList<>();
		feature_distribution = new int[SVCoordinate
				.values().length][distri_size];
		feature_averages = new float[SVCoordinate.values().length];

		try {
			BufferedReader analyzer_result = new BufferedReader(new FileReader(
					city_model.city_config.getSampleResultFile()));
			BufferedReader image_links = new BufferedReader(new FileReader(
					city_model.city_config.getSampleImageFile()));

			String line = analyzer_result.readLine();
			System.out.print("Street view analyzer: ");

			float[] detailed_vertices = new float[18
					* SVCoordinate.values().length
					* city_model.city_config.getSampleSize()];
			float[] detailed_colors = new float[24
					* SVCoordinate.values().length
					* city_model.city_config.getSampleSize()];

			float[] overview_vertices = new float[18
					* city_model.city_config.getSampleSize()];
			float[] overview_colors = new float[24
					* city_model.city_config.getSampleSize()];

			int sample_index = 0;

			while ((line = analyzer_result.readLine()) != null) {
				if (sample_index == city_model.city_config.getSampleSize())
					break;

				String image_link_line = image_links.readLine();
				String image_link = image_link_line.split(":")[0];

				String[] splits = line.split(",");
				float lat = Float.parseFloat(splits[1]);
				float lng = Float.parseFloat(splits[2]);
				SVSample result = new SVSample(image_link, lat, lng);

				float value;
				for (int i = 0; i < SVCoordinate.values().length; i++) {
					value = Float.parseFloat(splits[SVCoordinate.values()[i]
							.getIndexInResultFile()]);
					result.setValue(i, value);

					int distri_index = (int) value;
					if (distri_index < 0)
						distri_index = 0;

					if (distri_index >= feature_distribution[0].length)
						distri_index = feature_distribution[0].length - 1;

					feature_distribution[i][distri_index]++;
					max_distribution = Math.max(max_distribution,
							feature_distribution[i][distri_index]);

					feature_averages[i] = (feature_averages[i] * sample_index
							+ value) / (sample_index + 1);
				}

				result.orderResults();
				samples.add(result);

				// for (int i = 0; i < result.getOrderedResults().length; i++)
				// System.out.print(
				// SVComponent.values()[result.getOrderedIndex()[i]]
				// .getId() + ": "
				// + result.getOrderedResults()[i] + ";\t");
				// System.out.println();

				generateVertices(detailed_vertices, overview_vertices,
						sample_index, result);
				generateColors(detailed_colors, overview_colors, sample_index,
						result);

				sample_index++;
			}
			analyzer_result.close();
			image_links.close();

			System.out.println("loaded " + sample_index + " sample locations");
			DefaultGeometry g = DefaultGeometry.createVC(detailed_vertices,
					detailed_colors);
			detailed_sample_mesh = new DefaultMesh(Primitive.TRIANGLES,
					new ColorMaterial(new RGBA(1, 1, 1, 1), true), g);

			g = DefaultGeometry.createVC(overview_vertices, overview_colors);
			overview_sample_mesh = new DefaultMesh(Primitive.TRIANGLES,
					new ColorMaterial(new RGBA(1, 1, 1, 1), true), g);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		pcp_random_indices = new int[Math.min(1000, samples.size())];
		Random rand = new Random();

		for (int i = 0; i < pcp_random_indices.length; i++) {
			pcp_random_indices[i] = rand.nextInt(samples.size());
		}
	}

	public void pickResult(IController controller, float x, float y) {
		float lng = city_model.xToLng(x);
		float lat = city_model.yToLat(y);

		SVSample pick_sample = null;

		float min_dist = Float.MAX_VALUE, DIST_THRE = 100;
		for (SVSample result : city_model.getAnalyzer().getSamples()) {
			float dist = CoordUtil.distance(result.getLat(), lat,
					result.getLng(), lng, 0, 0);
			if (dist < min_dist && dist <= DIST_THRE) {
				min_dist = dist;

				pick_sample = result;
			}
		}

		if (pick_sample != null) {
			pick_sample.initMeshes(controller, city_model);
			pick_samples.add(pick_sample);
		}
	}

	public List<SVSample> getPickSamples() {
		return pick_samples;
	}

	public IMesh getDetailedSampleMesh() {
		return detailed_sample_mesh;
	}

	public IMesh getOverviewSampleMesh() {
		return overview_sample_mesh;
	}

	public List<SVSample> getSamples() {
		return samples;
	}

	public int[][] getFeatureDistribution() {
		return feature_distribution;
	}

	public float[] getFeatureAverage() {
		return feature_averages;
	}

	public int getMaxDistribution() {
		return max_distribution;
	}

	public int[] getPCPRandomIndices() {
		return pcp_random_indices;
	}

	private void generateVertices(float[] detailed_vertices,
			float[] overview_vertices, int index, SVSample result) {
		float x = city_model.lngToX(result.getLng());
		float y = city_model.latToY(result.getLat());

		Vec4 square = new Vec4(x - mesh_size / 2, y - mesh_size / 2, mesh_size,
				mesh_size);
		List<Vec4> tree_map_squares = TreeMap
				.squarify(result.getOrderedResults(), square);

		float z = 0.0001f;
		int start_index = index * 18 * SVCoordinate.values().length;
		for (int i = 0; i < tree_map_squares.size(); i++) {
			Vec4 sub_tree = tree_map_squares.get(i);
			float sub_x = sub_tree.x, sub_y = sub_tree.y,
					sub_width = sub_tree.z, sub_height = sub_tree.w;
			//@formatter:off
			float[] new_vertices = {
				sub_x, sub_y, z, 
				sub_x + sub_width, sub_y + sub_height, z, 
				sub_x, sub_y + sub_height, z,
				sub_x, sub_y, z, 
				sub_x + sub_width, sub_y , z, 
				sub_x + sub_width, sub_y + sub_height, z 
				};
			//@formatter:on
			fillArray(detailed_vertices, start_index, new_vertices);
			start_index += 18;
		}

		start_index = index * 18;
		//@formatter:off
		float[] new_vertices = {
			x - mesh_size / 2, y - mesh_size / 2, z, 
			x + mesh_size / 2, y + mesh_size / 2, z, 
			x - mesh_size / 2, y + mesh_size / 2, z,
			x - mesh_size / 2, y - mesh_size / 2, z, 
			x + mesh_size / 2, y - mesh_size / 2, z, 
			x + mesh_size / 2, y + mesh_size / 2, z,
			};
		//@formatter:on
		fillArray(overview_vertices, start_index, new_vertices);
	}

	private void generateColors(float[] detailed_colors,
			float[] overview_colors, int index, SVSample result)
			throws Exception {
		int[] ordered_indices = result.getOrderedIndex();
		int start_index = index * 24 * SVCoordinate.values().length;

		for (int sv_index = 0; sv_index < ordered_indices.length; sv_index++) {
			RGB color = SVCoordinate.values()[ordered_indices[sv_index]]
					.getColor();

			float[] rgba = new float[] { color.r, color.g, color.b, 1 };

			for (int i = 0; i < 6; i++) {
				fillArray(detailed_colors, start_index, rgba);
				start_index += 4;
			}
		}

		RGB color = SVCoordinate.values()[ordered_indices[0]].getColor();

		float[] rgba = new float[] { color.r, color.g, color.b, 1 };
//		float[] rgba = new float[] { 1, 1, 1, 1 };
		start_index = index * 24;
		for (int i = 0; i < 6; i++) {
			fillArray(overview_colors, start_index, rgba);
			start_index += 4;
		}
	}

	private void fillArray(float[] array, int start_index, float[] fill_array) {
		for (int i = 0; i < fill_array.length; i++) {
			array[start_index + i] = fill_array[i];
		}
	}
}
