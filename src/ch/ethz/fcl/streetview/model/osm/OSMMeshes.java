/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.model.osm;

import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import ch.ethz.fcl.basemap.Node;
import ch.ethz.fcl.basemap.osm.OSMRelation;
import ch.ethz.fcl.basemap.osm.OSMWay;
import ch.ethz.fcl.util.Triangulation2;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.IMesh.Primitive;
import ch.fhnw.ether.scene.mesh.IMesh.Queue;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.material.ColorMaterial;
import ch.fhnw.util.color.RGBA;

public class OSMMeshes {
	private OSMLoader osm;

	// meshes
	private IMesh road_mesh = null;
	private IMesh boundary_mesh = null;

	public OSMMeshes(OSMLoader osm) {
		this.osm = osm;
	}

	public void load() {
		generateWayMesh();
		generateBoundaryMesh();
	}

	private void generateWayMesh() {
		int way_count = 0;
		for (OSMWay way : osm.roads) {
			if (way == null)
				continue;
			way_count += way.getNodes().size();
		}

		float[] vertices = new float[way_count * 6];
		float[] colors = new float[way_count * 8];

		way_count = 0;
		for (OSMWay way : osm.roads) {
			if (way == null)
				continue;

			List<Node> nodes = way.getNodes();

			for (int index = 0; index < nodes.size() - 1; index++) {
				Node node = nodes.get(index);
				Node next_node = nodes.get(index + 1);

				vertices[way_count * 6] = osm.getModel().lngToX(node.getX());
				vertices[way_count * 6 + 1] = osm.getModel()
						.latToY(node.getY());
				vertices[way_count * 6 + 2] = 0f;

				vertices[way_count * 6 + 3] = osm.getModel()
						.lngToX(next_node.getX());
				vertices[way_count * 6 + 4] = osm.getModel()
						.latToY(next_node.getY());
				vertices[way_count * 6 + 5] = 0f;

				colors[way_count * 8] = 0.8f;
				colors[way_count * 8 + 1] = 0.8f;
				colors[way_count * 8 + 2] = 0.8f;
				colors[way_count * 8 + 3] = 0.2f;

				colors[way_count * 8 + 4] = 0.8f;
				colors[way_count * 8 + 5] = 0.8f;
				colors[way_count * 8 + 6] = 0.8f;
				colors[way_count * 8 + 7] = 0.2f;
				way_count++;
			}
		}

		DefaultGeometry g = DefaultGeometry.createVC(vertices, colors);
		road_mesh = new DefaultMesh(Primitive.LINES,
				new ColorMaterial(RGBA.BLACK, true), g, Queue.DEPTH);

		road_mesh.setName("ways");
	}

	private void generateBoundaryMesh() {
		float[] all_vertices = null;

		for (OSMRelation boundary : osm.admin_boundary) {
			float[] sorted_vertices = boundary.getSortedVertices();

			float[] bounded_vertices = new float[sorted_vertices.length];
			for (int i = 0; i < sorted_vertices.length; i++) {
				if (i % 3 == 0)
					bounded_vertices[i] = osm.getModel()
							.lngToX(sorted_vertices[i]);
				else if (i % 3 == 1)
					bounded_vertices[i] = osm.getModel()
							.latToY(sorted_vertices[i]);
				else if (i % 3 == 2)
					bounded_vertices[i] = 0;
			}

			all_vertices = ArrayUtils.addAll(all_vertices,
					Triangulation2.triangulate(bounded_vertices));
		}

		for (OSMWay way : osm.island_boundary) {
			if (way == null)
				continue;

			float[] way_vertices = null;
			List<Node> nodes = way.getNodes();

			for (int index = 0; index < nodes.size(); index++) {
				Node node = nodes.get(index);

				way_vertices = ArrayUtils.addAll(way_vertices,
						new float[] { osm.getModel().lngToX(node.getX()),
								osm.getModel().latToY(node.getY()), 0 });
			}

			if (way_vertices == null)
				continue;

			all_vertices = ArrayUtils.addAll(all_vertices,
					Triangulation2.triangulate(way_vertices));
		}

		DefaultGeometry g = DefaultGeometry.createV(all_vertices);
		boundary_mesh = new DefaultMesh(Primitive.TRIANGLES,
				// new ColorMaterial(new RGBA(0.4f, 0.4f, 0.4f, 0.5f)),
				new ColorMaterial(new RGBA(0.9f, 0.9f, 0.9f, 0.7f)), g);
		boundary_mesh.setName("boundary");
	}

	public IMesh getRoadMesh() {
		return road_mesh;
	}

	public IMesh getBoundaryMesh() {
		return boundary_mesh;
	}

}
