/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.model.osm;

import ch.ethz.fcl.basemap.Edge;
import ch.ethz.fcl.basemap.Node;
import ch.ethz.fcl.basemap.osm.OSMHighway;
import ch.ethz.fcl.basemap.osm.OSMRelation;
import ch.ethz.fcl.basemap.osm.OSMWay;
import ch.ethz.fcl.spatialindex.object2d.Point2D;
import ch.ethz.fcl.util.CoordUtil;
import ch.ethz.fcl.util.MathUtil;
import ch.ethz.fcl.util.VectorUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import ch.ethz.fcl.streetview.config.SVGeneralConfig;
import ch.ethz.fcl.streetview.model.SVCityModel;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh.Primitive;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.material.PointMaterial;
import ch.fhnw.util.color.RGBA;

public class OSMLoader {
	private SVCityModel model;

	public OSMMeshes meshes = null;

	public float min_lat, max_lat, min_lon, max_lon;
	public float scene_min_x = -0.95f, scene_max_x = 0.95f;
	public float scene_min_y, scene_max_y;

	private Map<String, Node> node_map = null; // key: node id; value: node
	private Map<String, OSMWay> way_map = null; // key: way id; value way

	private OSMWay cur_road;
	protected List<OSMWay> roads = null;
	private List<Edge> edges = null;

	protected List<OSMRelation> admin_boundary = null;
	protected List<OSMWay> island_boundary = null;
	protected OSMRelation cur_relation;
	List<Point2D> sample_points = new ArrayList<Point2D>();
	Map<Point2D, Edge> sample_points_edge_map = new HashMap<Point2D, Edge>();
	Map<String, Integer> road_count_map = new HashMap<String, Integer>();

	public OSMLoader(SVCityModel model) {
		this.model = model;
		this.meshes = new OSMMeshes(this);

		node_map = new HashMap<String, Node>();
		way_map = new HashMap<String, OSMWay>();

		admin_boundary = new ArrayList<OSMRelation>();
		island_boundary = new ArrayList<OSMWay>();
		roads = new ArrayList<OSMWay>();
		edges = new ArrayList<Edge>();
	}

	public void load() {
		readOSM();
		meshes.load();

		node_map = null;
		way_map = null;
	}

	public void findNearestNode(IController controller, float x, float y) {
		float min_dist = Float.MAX_VALUE;
		Node nearest_node = null;

		for (Edge edge : edges) {
			float node_x = model.lngToX(edge.getStart().getX());
			float node_y = model.latToY(edge.getStart().getY());

			float dist = MathUtil.distance(x, y, node_x, node_y);
			if (dist < min_dist) {
				min_dist = dist;
				nearest_node = edge.getStart();
			}
		}

		Stack<Node> node_stack = new Stack<Node>();
		node_stack.push(nearest_node);

		List<Edge> visited_edges = new ArrayList<Edge>();
		Node last_sample_node = nearest_node;
		float pre_sum_dist = 0;
		Point2D p0 = new Point2D(nearest_node.getX(), nearest_node.getY());
		sample_points.add(p0);
		sample_points_edge_map.put(p0, nearest_node.getConnectedEdges().get(0));

		while (!node_stack.isEmpty()) {
			Node node = node_stack.pop();
			node.visisted = true;

			if (node.equals(nearest_node))
				System.out.println("connected edge size "
						+ node.getConnectedEdges().size());

			for (Edge node_edge : node.getConnectedEdges()) {
				if (!node_edge.visited) {
					if (node_edge.getStart().equals(node)
							&& !node_edge.getEnd().visisted) {
						node_stack.push(node_edge.getEnd());
						node_edge.getEnd().parent_nodes.add(node);
					} else if (!node_edge.getStart().visisted) {
						node_stack.push(node_edge.getStart());
						node_edge.getStart().parent_nodes.add(node);
					}

					visited_edges.add(node_edge);
					node_edge.visited = true;
				}
			}

			for (Node parent_node : node.parent_nodes) {
				if (!last_sample_node.equals(parent_node)) {
					last_sample_node = parent_node;
					pre_sum_dist = 0;
				}

				Edge cur_edge = null;
				for (Edge e : node.getConnectedEdges()) {
					if ((e.getStart().equals(node)
							&& e.getEnd().equals(parent_node))
							|| (e.getEnd().equals(node)
									&& e.getStart().equals(parent_node))) {
						cur_edge = e;
						break;
					}
				}

				float dist = CoordUtil.distance(last_sample_node.getY(),
						node.getY(), last_sample_node.getX(), node.getX(), 0,
						0);

				if (dist + pre_sum_dist >= SVGeneralConfig.SAMPLE_DIST) {
					float sum_dist = dist + pre_sum_dist;
					while (sum_dist >= SVGeneralConfig.SAMPLE_DIST) {
						float sample_x = MathUtil.map(
								sum_dist - SVGeneralConfig.SAMPLE_DIST, 0, dist,
								node.getX(), last_sample_node.getX());
						float sample_y = MathUtil.map(
								sum_dist - SVGeneralConfig.SAMPLE_DIST, 0, dist,
								node.getY(), last_sample_node.getY());

						sum_dist -= SVGeneralConfig.SAMPLE_DIST;

						Point2D new_p = new Point2D(sample_x, sample_y);
						sample_points.add(new_p);

						sample_points_edge_map.put(new_p, cur_edge);
					}

					pre_sum_dist = sum_dist;
				} else {
					pre_sum_dist += dist;
				}
			}

			last_sample_node = node;
		}

		System.out.println("flood fill " + visited_edges.size() + " edges "
				+ sample_points.size() + " points");

		float[] sample_vertices = new float[sample_points.size() * 3];
		int count = 0;
		for (Point2D sample_p : sample_points) {
			sample_vertices[count] = model.lngToX(sample_p.getX());
			sample_vertices[count + 1] = model.latToY(sample_p.getY());
			sample_vertices[count + 2] = 0.0001f;

			count += 3;
		}

		DefaultGeometry sample_g = DefaultGeometry.createV(sample_vertices);
		DefaultMesh sample_p = new DefaultMesh(Primitive.POINTS,
				new PointMaterial(new RGBA(0f, 0f, 1f, 1f), 5), sample_g);
		controller.getScene().add3DObjects(sample_p);

		exportSamplePoints();
	}

	private void exportSamplePoints() {
		road_count_map = new HashMap<String, Integer>();
		try {
			FileWriter fw = new FileWriter(
					new File(SVGeneralConfig.SAMPLE_FILE_LOCAITON));
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write(
					"index, latitude, longitude, road_id, road_count, heading, link\n");

			for (int i = 0; i < sample_points.size(); i++) {
				Point2D p = sample_points.get(i);

				Edge edge = sample_points_edge_map.get(p);
				Integer road_count = road_count_map.get(edge.getId());
				if (road_count == null)
					road_count = 0;

				float x = edge.getEnd().getX() - edge.getStart().getX();
				float y = edge.getEnd().getY() - edge.getStart().getY();
				int heading = VectorUtil.Rd(x, y);

				// String link =
				// "http://api.map.baidu.com/panorama/v2?ak=U1u8G53x2q27ahAxbPkxiLktprOBIcWD&width=640&height=480&location="
				// + p.getX() + "," + p.getY()
				// + "&coordtype=wgs84ll&heading=" + heading
				// + "&pitch=0&fov=90";

				bw.write(i + ", " + p.getY() + ", " + p.getX() + ", "
						+ edge.getId() + ", " + road_count + ", " + heading
						+ "\n");

				road_count_map.put(edge.getId(), road_count + 1);
			}

			bw.flush();
			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readOSM() {
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();

		try {
			InputStream is = getClass()
					.getResource(model.city_config.getOSMFile()).openStream();

			try {
				SAXParser parser = parserFactory.newSAXParser();

				parser.parse(is, new DefaultHandler() {
					@Override
					public InputSource resolveEntity(String publicId,
							String systemId) {
						return new InputSource(new StringReader(""));
					}

					@Override
					public void startElement(String uri, String localName,
							String name, Attributes attributes) {
						switch (name) {
						case "bounds":
							readBound(attributes);
							break;

						case "node":
							// attributes: id, x, y, type
							String id = attributes.getValue("id");
							String lat = attributes.getValue("lat");
							String lon = attributes.getValue("lon");

							float fx = Float.parseFloat(lon);
							float fy = Float.parseFloat(lat);

							Node node = new Node(id, fx, fy);
							node_map.put(id, node);
							break;

						case "way":
							int way_id = Integer
									.parseInt(attributes.getValue("id"));
							cur_road = new OSMWay(way_id);
							break;

						case "relation":
							int relation_id = Integer
									.parseInt(attributes.getValue("id"));
							cur_relation = new OSMRelation(relation_id);
							break;

						case "nd":
							String node_ref = attributes.getValue("ref");

							Node nd = node_map.get(node_ref);
							if (cur_road != null && nd != null)
								cur_road.addNode(nd);
							break;

						case "tag":
							String k = attributes.getValue("k");

							if (k != null) {
								String v = attributes.getValue("v");
								if (cur_road != null) {
									cur_road.setAttribute(k, v);
								}

								if (cur_road != null && k.equals("place")
										&& v.equals("island")) {
									island_boundary.add(cur_road);
								}

								if (cur_road != null && k.equals("natural")
										&& (v.equals("coastline")
												|| v.equals("beach"))) {
									island_boundary.add(cur_road);
								}

								if (cur_relation != null) {
									cur_relation.setAttribute(k, v);
								}
							}
							break;

						case "member":
							String type = attributes.getValue("type");
							String ref = attributes.getValue("ref");
							String role = attributes.getValue("role");

							if (cur_relation != null) {
								if (type.equals("way")
										&& role.equals("outer")) {
									OSMWay way = way_map.get(ref);
									if (way != null)
										cur_relation.addWay(way);
								} else if (type.equals("node")) {
									Node relation_node = node_map.get(ref);
									if (relation_node != null)
										cur_relation.addNode(relation_node);
								}
							}
							break;
						}
					}

					@Override
					public void endElement(String uri, String localName,
							String name) {
						switch (name) {
						case "way":
							if (cur_road == null)
								return;
							if (cur_road.getHighwayType() == OSMHighway.ROADS) {
								roads.add(cur_road);

								// generate edges for the roads
								for (int i = 0; i < cur_road.getNodes().size()
										- 1; i++) {
									Node n1 = cur_road.getNodes().get(i);
									Node n2 = cur_road.getNodes().get(i + 1);

									edges.add(new Edge(cur_road.getId() + "",
											n1, n2));
								}
							}

							way_map.put(cur_road.getId() + "", cur_road);

							cur_road = null;
							break;

						case "relation":
							if (cur_relation == null)
								return;

							if (!cur_relation.ignore()) {
								admin_boundary.add(cur_relation);
							}
							cur_relation = null;
							break;
						}
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("osm loaded " + node_map.size() + " nodes, "
				+ roads.size() + " roads");
	}

	private void readBound(Attributes attributes) {
		min_lat = Float.parseFloat(attributes.getValue("minlat"));
		max_lat = Float.parseFloat(attributes.getValue("maxlat"));
		min_lon = Float.parseFloat(attributes.getValue("minlon"));
		max_lon = Float.parseFloat(attributes.getValue("maxlon"));

		float x_dist = CoordUtil.distance(min_lat, min_lat, min_lon, max_lon, 0,
				0);
		float y_dist = CoordUtil.distance(min_lat, max_lat, min_lon, min_lon, 0,
				0);

		float yx_ratio = y_dist / x_dist;
		scene_min_y = -yx_ratio * (scene_max_x - scene_min_x) / 2;
		scene_max_y = yx_ratio * (scene_max_x - scene_min_x) / 2;

		System.out.println("bounds: [" + min_lat + " " + max_lat + ", " + " "
				+ min_lon + " " + max_lon + "]");
		System.out.println("view bounds: [" + scene_min_x + " " + scene_max_x
				+ ", " + " " + scene_min_y + " " + scene_max_y + "]");
	}

	public SVCityModel getModel() {
		return model;
	}

	public OSMMeshes getMeshes() {
		return meshes;
	}
}
