/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.model;

import ch.ethz.fcl.streetview.config.SVCityConfig;
import ch.ethz.fcl.streetview.config.SVGeneralConfig;
import ch.ethz.fcl.streetview.model.gsv_result.SVAnalyzer;
import ch.ethz.fcl.streetview.model.osm.OSMLoader;
import ch.ethz.fcl.streetview.ui.render.SVMapRenderer;
import ch.ethz.fcl.util.CoordUtil;
import ch.ethz.fcl.util.MathUtil;
import ch.ethz.fcl.util.ProjectionUtil;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.view.IView;

public class SVCityModel {
	public SVCityConfig city_config;

	private OSMLoader osm = new OSMLoader(this);
	private SVAnalyzer analyzer = new SVAnalyzer(this);

	private float scale_world_dist = 0;

	public SVCityModel(SVCityConfig city_config) {
		this.city_config = city_config;

		osm.load();
		if (!SVGeneralConfig.GENERATE_SAMPLE_LOCATIONS)
			analyzer.load();
	}

	public void addToScene(IScene scene) {
		if (SVGeneralConfig.SHOW_ROAD) {
			if (!scene.get3DObjects().contains(osm.getMeshes().getRoadMesh()))
				scene.add3DObject(osm.getMeshes().getRoadMesh());
		}

		if (!scene.get3DObjects().contains(osm.getMeshes().getBoundaryMesh()))
			scene.add3DObjects(osm.getMeshes().getBoundaryMesh());

		if (!SVGeneralConfig.GENERATE_SAMPLE_LOCATIONS) {
			if (!scene.get3DObjects()
					.contains(analyzer.getOverviewSampleMesh()))
				scene.add3DObject(analyzer.getOverviewSampleMesh());
		}
	}

	public void removeFromScene(IScene scene) {
		if (scene.get3DObjects().contains(osm.getMeshes().getBoundaryMesh()))
			scene.remove3DObject(osm.getMeshes().getBoundaryMesh());

		if (scene.get3DObjects().contains(analyzer.getOverviewSampleMesh()))
			scene.remove3DObject(analyzer.getOverviewSampleMesh());
	}

	public void showDetailedSampleMesh(IScene scene) {
		if (scene.get3DObjects().contains(analyzer.getOverviewSampleMesh())) {
			scene.remove3DObject(analyzer.getOverviewSampleMesh());
			scene.add3DObject(analyzer.getDetailedSampleMesh());
		}

		if (SVGeneralConfig.SHOW_ROAD) {
			if (!scene.get3DObjects().contains(osm.getMeshes().getRoadMesh()))
				scene.add3DObject(osm.getMeshes().getRoadMesh());
		}
	}

	public void showOverviewSampleMesh(IScene scene) {
		if (scene.get3DObjects().contains(analyzer.getDetailedSampleMesh())) {
			scene.remove3DObject(analyzer.getDetailedSampleMesh());
			scene.add3DObject(analyzer.getOverviewSampleMesh());
		}
	}

	public void hide(IScene scene, IMesh... mesh) {
		scene.remove3DObjects(mesh);
	}

	public float xToLng(float x) {
		return MathUtil.map(x - city_config.getCenter().x, osm.scene_min_x,
				osm.scene_max_x, osm.min_lon, osm.max_lon);
	}

	public float yToLat(float y) {
		return MathUtil.map(y - city_config.getCenter().y, osm.scene_min_y,
				osm.scene_max_y, osm.min_lat, osm.max_lat);
	}

	public float lngToX(float lng) {
		return MathUtil.map(lng, osm.min_lon, osm.max_lon,
				osm.scene_min_x + city_config.getCenter().x,
				osm.scene_max_x + city_config.getCenter().x);
	}

	public float latToY(float lat) {
		return MathUtil.map(lat, osm.min_lat, osm.max_lat,
				osm.scene_min_y + city_config.getCenter().y,
				osm.scene_max_y + city_config.getCenter().y);
	}

	public void updateScaleWorldDist(IView view) {
		float scene_length = ProjectionUtil.getSceneDistance(view,
				SVMapRenderer.SCALE_LENGTH);
		scale_world_dist = getWorldDist(scene_length);
	}

	public float getScaleWorldDist() {
		return scale_world_dist;
	}

	public float getWorldDist(float scene_dist) {
		float max_scene_dist = MathUtil.distance(osm.scene_min_x,
				osm.scene_min_y, osm.scene_max_x, osm.scene_max_y);
		float max_world_dist = CoordUtil.distance(osm.min_lat, osm.max_lat,
				osm.min_lon, osm.max_lon, 0, 0);
		return max_world_dist / max_scene_dist * scene_dist;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof SVCityModel) {
			SVCityModel model = (SVCityModel) o;
			return model.getCityConfig().getCity()
					.equals(city_config.getCity());
		}
		return false;
	}

	public OSMLoader getOSM() {
		return osm;
	}

	public SVAnalyzer getAnalyzer() {
		return analyzer;
	}

	public SVCityConfig getCityConfig() {
		return city_config;
	}
}