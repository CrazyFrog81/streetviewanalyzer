/*
 * Copyright (C) 2010 - 2016 ETH Zurich
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Zeng Wei (zeng@arch.ethz.ch)
 */

package ch.ethz.fcl.streetview.model;

import ch.ethz.fcl.streetview.config.SVCityConfig;
import ch.ethz.fcl.streetview.ui.ViewSide;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.view.IView;
import ch.fhnw.util.math.Vec3;

public class SVModel {
	private SVCityModel[] city_models = new SVCityModel[SVCityConfig
			.values().length];

	private SVCityModel left_city, right_city = null;

	public SVCityModel selected_city = null;

	public SVModel() {
		int count = 0;
		for (SVCityConfig city_config : SVCityConfig.values()) {
			city_models[count++] = new SVCityModel(city_config);
		}

		left_city = city_models[0];
		right_city = city_models[1];
	}

	public void changeCity(IScene scene, IView view, ViewSide side,
			SVCityModel city) {
		if (side == ViewSide.Left) {
			if (!left_city.equals(right_city))
				left_city.removeFromScene(scene);
			left_city = city;
			left_city.addToScene(scene);
		} else {
			if (!left_city.equals(right_city))
				right_city.removeFromScene(scene);
			right_city = city;
			right_city.addToScene(scene);
		}
		view.getController().setCamera(view,
				getCameraLookingDown(city.getCityConfig().getCenter()));
	}

	private Camera getCameraLookingDown(Vec3 cam_pos) {
		return new Camera(cam_pos, new Vec3(cam_pos.x, cam_pos.y, 0));
	}

	public void addToScene(IScene scene) {
		left_city.addToScene(scene);
//		right_city.addToScene(scene);
	}

	public SVCityModel[] getCityModels() {
		return city_models;
	}

	public SVCityModel getLeftCity() {
		return left_city;
	}

	public SVCityModel getRightCity() {
		return right_city;
	}
}
