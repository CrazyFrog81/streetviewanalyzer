/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.config;

public interface SVGeneralConfig {
	public static boolean GENERATE_SAMPLE_LOCATIONS = true;
	public static boolean SHOW_ROAD = true;
	public static String SAMPLE_FILE_LOCAITON = "src/resources/Shanghai_Sample_Locations.csv";
	public static int SAMPLE_DIST = 50;

	public static String DATA_FOLDER = "/Users/zengwei/Documents/2_Data/GoogleStreetView/";
}
