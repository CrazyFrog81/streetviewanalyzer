/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.config;

import ch.fhnw.util.color.RGB;

public enum SVCoordinate {
	// @formatter:off
	GREEN(3, "green", new RGB(77 / 255.0f, 175 / 255.0f, 74 / 255.0f)), 
	SKY(4, "sky", new RGB(55 / 255.0f, 126 / 255.0f, 184 / 255.0f)), 
	BUILDING(5, "building", new RGB(240 / 255.0f, 2 / 255.0f, 127 / 255.0f)), 
	ROAD(6,	"road", new RGB(190 / 255.0f, 174 / 255.0f, 212 / 255.0f)), 
	CAR(7, "vehicle", new RGB(191 / 255.0f, 91 / 255.0f, 23 / 255.0f)), 
	OTHERS(8, "others", new RGB(102 / 255.0f, 102 / 255.0f, 102 / 255.0f));
	//@formatter:on

	SVCoordinate(int index, String id, RGB color) {
		this.index = index;
		this.id = id;
		this.color = color;
	}

	public int getIndexInResultFile() {
		return index;
	}

	public String getId() {
		return id;
	}

	public RGB getColor() {
		return color;
	}

	int index; // column index in the result csv file
	String id;
	RGB color;
	public static final int START_INDEX = 3;
}
