/*
* Copyright (C) 2010 - 2016 ETH Zurich
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Zeng Wei (zeng@arch.ethz.ch)
*/

package ch.ethz.fcl.streetview.config;

import ch.fhnw.util.math.Vec3;

public enum SVCityConfig {
	//@formatter:off
	// Singapore
	Singapore("/osm/shanghai.osm",
			  "/Users/zengwei/Documents/2_Data/GoogleStreetView/Singapore/results.csv",
			  "/Users/zengwei/Documents/2_Data/GoogleStreetView/Singapore/singapore.image_list.txt",
			  182792,
			  new Vec3(0, 0, 2.5),
			  "src/resources/preview/singapore.png",
			  "Singapore",
			  "719.1 sq km"),
	// Hong Kong
	HongKong("/osm/HongKong.osm",
			 "/Users/zengwei/Documents/2_Data/GoogleStreetView/HongKong/results.csv",
			 "/Users/zengwei/Documents/2_Data/GoogleStreetView/HongKong/hongkong.image_list.txt",
			 147512,
			 new Vec3(4, 4, 1.5),
			 "src/resources/preview/hongkong.png",
			 "Hong Kong",
			 "1,104 sq km");
	// London
//	London("/osm/London.osm",
//			"/Users/zengwei/Documents/2_Data/GoogleStreetView/London/results.csv",
//			"/Users/zengwei/Documents/2_Data/GoogleStreetView/London/London.image_list.txt",
//			 684987,
//			 new Vec3(8, 8, 1.8),
//			 "src/resources/preview/london.png",
//			 "London",
//			 "1,572 sq km"),
	// NYC
//	NYC("/osm/NYC.osm",
//		"/Users/zengwei/Documents/2_Data/GoogleStreetView/HongKong/results.csv",
//		"/Users/zengwei/Documents/2_Data/GoogleStreetView/HongKong/hongkong.image_list.txt",
//		147512,
//		new Vec3(12, 12, 2.27),
//		"src/resources/preview/NYC.png",
//		"New York City",
//		"789 sq km");
	//@formatter:on

	private SVCityConfig(String OSM_file, String result_file,
			String image_link_file, int sample_size, Vec3 center,
			String preview_img_link, String city, String size) {
		this.OSM_file = OSM_file;
		this.sample_result_file = result_file;
		this.sample_image_file = image_link_file;
		this.sample_size = sample_size;
		this.preview_img_link = preview_img_link;
		this.center = center;
		this.city = city;
		this.size = size;
	}

	private String OSM_file, sample_result_file, sample_image_file;
	private int sample_size;
	private Vec3 center;
	private String preview_img_link;
	private String city, size;

	public String getOSMFile() {
		return OSM_file;
	}

	public String getSampleResultFile() {
		return sample_result_file;
	}

	public String getSampleImageFile() {
		return sample_image_file;
	}

	public int getSampleSize() {
		return sample_size;
	}

	public Vec3 getCenter() {
		return center;
	}

	public String getPreviewImgLink() {
		return preview_img_link;
	}

	public String getCity() {
		return city;
	}

	public String getSize() {
		return size;
	}
}